﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlServerCe;
using System.Globalization;
using WindowsApplications.EventControls;

namespace WindowsApplications.Execute
{
    public class Executing : WindowsApplications.Connections.Connection, IDisposable
    {
        #region Instance

        private static Executing _Instance = new Executing();
        public static Executing Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Executing();
                }
                return _Instance;
            }
        }

        #endregion

        #region 0. getDateTime

        public DateTime GetDateServer()
        {
            try
            {
                DateTime dtpDateTime = DateTime.Now;

                string sql = "SELECT GETDATE() AS getDateTime";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                DataTable dtDateTime = (DataTable)dbManager.ExecuteToDataTable(command);
                dbManager.Close();

                return (DateTime)dtDateTime.Rows[0]["getDateTime"];
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region 1. [1.1.2] Tab DistributionRet

        public System.Data.DataTable getDistributionRet()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT create_date, empty_or_full, cust_id, doc_no, serial_number, vehicle FROM Distribution_Ret";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 2. [1.2] Tab GIDelivery

        public System.Data.DataTable getGIDelivery()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT create_date, do_no, serial_number FROM GI_Delivery";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 3. [1.4.1] Tab GIRefIOEmpty

        public System.Data.DataTable getGIRefIOEmpty()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT create_date, do_no, serial_number FROM GI_Ref_IO_Empty";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 4. [1.5.3] Tab GRFromProduction

        public System.Data.DataTable getGRFromProduction()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT create_date, pre_order, batch, serial_number FROM GR_Production";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 5. [1] Tab Warehouse

        public System.Data.DataTable getWarehouse()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT convert(nvarchar(10), wh.create_date, 103) as cDate, convert(nvarchar(10), wh.create_date, 108) as cTime, wh.item_code, wh.sell_order_no, wh.cylinder_sn, wh.cust_id as customer_code, cu.cust_name as customer_name FROM warehouse wh inner join customer cu on wh.cust_id = cu.cust_id ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 6. [2] Tab Delivery

        public System.Data.DataTable getDelivery()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT convert(nvarchar(10), de.create_date, 103) as cDate, convert(nvarchar(10), de.create_date, 108) as cTime, de.item_code, de.factory_code, de.cylinder_sn, de.cust_id, cu.cust_name, de.vehicle, de.empty_or_full FROM delivery de inner join customer cu on de.cust_id = cu.cust_id ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 7. [3] Tab New Barcode

        public System.Data.DataTable getNewBarcode()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT convert(nvarchar(10), create_date, 103) as cDate, convert(nvarchar(10), create_date, 108) as cTime, item_code, notes, scan_number FROM otherscans ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 8. Insert Log

        public bool Insert_Log(string error_code, string error_message, string page_form, string method_name)
        {
            bool _result = false;
            try
            {

                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO Action_Log (id, error_code, error_message, page_form, method_name, create_date)  ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}', '{3}', '{4}', getdate()) ", Guid.NewGuid().ToString().ToUpper().Trim(), error_code, error_message.Replace("'", ""), page_form, method_name));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;

                dbManager.Close();
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }

        #endregion

        #region 9. Activate Licence

        public string CheckExpireSoftware()
        {
            string connti = "FALSE";
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

                DataTable dt_licence = new DataTable();

                string sql = "SELECT TOP(1) expire_date " +
                             "FROM t_setup ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_licence = dbManager.ExecuteToDataTable(command);
                dbManager.Close();

                if (!dt_licence.IsNullOrNoRows())
                {
                    #region Has data

                    string _expire_date = dt_licence.Rows[0][0].ToString().Trim();
                    if (string.IsNullOrEmpty(_expire_date))
                    {
                        connti = "FALSE";
                    }
                    else if (!string.IsNullOrEmpty(_expire_date))
                    {
                        string _decry_expire_date = Convert.ToString(Convert.ToInt64(_expire_date) - 39335472);
                        if (_decry_expire_date.Trim() == "30000101")
                        {
                            connti = "LIFETIME";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_decry_expire_date))
                            {
                                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);

                                if (Convert.ToInt64(_decry_expire_date) < Convert.ToInt64(_crrent_date))
                                    connti = "EXPIRE";
                                else if (Convert.ToInt64(_decry_expire_date) > Convert.ToInt64(_crrent_date))
                                    connti = "TRUE";
                                else
                                    connti = "FALSE";
                            }
                            else
                            {
                                connti = "FALSE";
                            }
                        }
                    }

                    #endregion
                }
                else
                {
                    #region Not has data

                    connti = "FALSE";

                    #endregion
                }
            }
            catch (Exception)
            {
                connti = "FALSE";
            }

            return connti;
        }
        public bool ActivateLicence(string _licence_key)
        {
            bool result = false;
            try
            {
                string text = "UPDATE t_setup SET expire_date = '" + _licence_key.Trim() + "', update_date = GETDATE() ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                dbManager.Open();
                dbManager.ExecuteNonQuery(sqlCeCommand);
                dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
        public string ValidationLicence(string _licence_keys)
        {
            string Result = string.Empty;
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
                string _licence_key = Convert.ToString((Convert.ToInt64(_licence_keys) - 39335472));
                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);
                if (!string.IsNullOrEmpty(_licence_key))
                {
                    if (_licence_key.Trim() == "30000101")
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            Result = "TRUE";
                        }
                    }
                    else if (Convert.ToInt32(_licence_key) < Convert.ToInt32(_crrent_date))
                    {
                        Result = "TRUE_EXPIRED";
                    }
                    else if (_licence_key.IndexOf("2016") >= 0)
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            DateTime dTime = DateTime.ParseExact(_licence_key, "yyyyMMdd", null);
                            string display_date = dTime.ToLongDateString();
                            Result = "TRUE_EXPRIE|" + display_date;
                        }
                    }
                    else
                    {
                        Result = "FALSE";
                    }
                }
            }
            catch (Exception)
            {
                Result = "FALSE";
            }

            return Result;
        }
        public string getDateExpireSoftware()
        {
            string connti = "FALSE";
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

                DataTable dt_licence = new DataTable();

                string sql = "SELECT TOP(1) expire_date " +
                             "FROM t_setup ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_licence = dbManager.ExecuteToDataTable(command);
                dbManager.Close();

                if (!dt_licence.IsNullOrNoRows())
                {
                    #region Has data

                    string _expire_date = dt_licence.Rows[0][0].ToString().Trim();
                    if (string.IsNullOrEmpty(_expire_date))
                    {
                        connti = "FALSE";
                    }
                    else if (!string.IsNullOrEmpty(_expire_date))
                    {
                        string _decry_expire_date = Convert.ToString(Convert.ToInt64(_expire_date) - 39335472);
                        if (_decry_expire_date.Trim() == "30000101")
                        {
                            connti = "LIFETIME";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_decry_expire_date))
                            {
                                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);

                                if (Convert.ToInt64(_decry_expire_date) < Convert.ToInt64(_crrent_date))
                                {
                                    connti = "EXPIRE";
                                }
                                else if (Convert.ToInt64(_decry_expire_date) > Convert.ToInt64(_crrent_date))
                                {
                                    DateTime dTime = DateTime.ParseExact(_decry_expire_date, "yyyyMMdd", null);
                                    string display_date = dTime.ToLongDateString();
                                    connti = display_date;
                                }
                                else
                                {
                                    connti = "FALSE";
                                }
                            }
                            else
                            {
                                connti = "FALSE";
                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception)
            {
                connti = "FALSE";
            }

            return connti;
        }
        public string getSignInOut()
        {
            string connti = "FALSE";
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

                DataTable dt_licence = new DataTable();

                string sql = "select [password] " +
                             "from t_password where screen_type = 'IS_USED' ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_licence = dbManager.ExecuteToDataTable(command);
                dbManager.Close();

                if (!dt_licence.IsNullOrNoRows())
                {
                    #region Has data

                    string _is_sign = dt_licence.Rows[0][0].ToString().Trim();
                    if (string.IsNullOrEmpty(_is_sign))
                    {
                        connti = "FALSE";
                    }
                    else if (!string.IsNullOrEmpty(_is_sign))
                    {
                        if (_is_sign.Trim() == "Y")
                            connti = "SIGN_IN";
                        else if(_is_sign.Trim() == "N")
                            connti = "SIGN_OUT";
                    }

                    #endregion
                }
            }
            catch (Exception)
            {
                connti = "FALSE";
            }

            return connti;
        }

        #endregion

        #region 10. Get Config Sign On

        public string getConfigSignOn(string _screen_type)
        {
            string text = string.Empty;
            string result;
            try
            {
                DataTable dataTable = new DataTable();
                string empty = string.Empty;
                string text2 = "select password from t_password WHERE (screen_type = '" + _screen_type + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text2.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                if (!dataTable.IsNullOrNoRows())
                    text = Convert.ToString(dataTable.Rows[0]["password"]);
                else
                    text = "0";

                result = text;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}