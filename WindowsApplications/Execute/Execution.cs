﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Text;

namespace WindowsApplications.Execute
{
    public class Execution : IDisposable
    {
        #region Member

        private SqlCeEngine sqlCEEngine;
        string connectionString = string.IsNullOrEmpty(ConfigurationManager.AppSettings["conn"].ToString()) ? @"Data Source=C:\Data Base SDF\TJG.sdf;Default Lock Timeout = 30000;Max Database Size = 2560;Password=pass;Persist Security Info=True" : ConfigurationManager.AppSettings["conn"].ToString().Trim();
        string connectionStringLogging = string.IsNullOrEmpty(ConfigurationManager.AppSettings["connLogging"].ToString()) ? @"Data Source=C:\Data Base SDF\Logging.sdf;Default Lock Timeout = 30000;Max Database Size = 2560;Password=pass;Persist Security Info=True" : ConfigurationManager.AppSettings["connLogging"].ToString().Trim();

        #endregion

        #region Constructor

        public Execution()
        {
            
        }

        #endregion

        #region 0. getDateTime

        public DateTime GetDateServer()
        {
            try
            {
                DateTime dtpDateTime = DateTime.Now;
                System.Data.DataTable dataTable = new System.Data.DataTable();
                SqlCeConnection conn = new SqlCeConnection(connectionString);
                sqlCEEngine = new SqlCeEngine(connectionString);
                sqlCEEngine.Upgrade();
                conn.Open();

                SqlCeCommand command = conn.CreateCommand();
                command.CommandText = "SELECT GETDATE() AS getDateTime";
                command.CommandType = System.Data.CommandType.Text;
                SqlCeDataReader dr = command.ExecuteReader();
                dataTable.Load(dr);
                DataTable dtDateTime = dataTable;

                return (DateTime)dtDateTime.Rows[0]["getDateTime"];
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region 1. Search Logging By Criteria 

        public System.Data.DataTable getLoggingBycriteria(string startDate, string endDate, string errorCode)
        {
            System.Data.DataTable result;
            try
            {
                string _startDate = string.Empty;
                string _endDate = string.Empty;
                SqlCeConnection conn = new SqlCeConnection(connectionStringLogging);
                conn.Open();
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = string.Empty;
                if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) && !string.IsNullOrEmpty(errorCode))
                {
                    //_startDate = startDate.Value.ToString("dd-MM-yy");
                    //_endDate = endDate.Value.ToString("dd-MM-yy");
                    //text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,5) >= @START_DATE AND CONVERT(nvarchar(11),create_date,5) <= @END_DATE) AND error_code LIKE '%"+ errorCode +"%' ";
                    text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,103) >= @START_DATE AND CONVERT(nvarchar(11),create_date,103) <= @END_DATE) AND error_code LIKE '%" + errorCode + "%' ORDER BY create_date DESC ";
                }
                else if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    //_startDate = startDate.Value.ToString("dd-MM-yy");
                    //_endDate = endDate.Value.ToString("dd-MM-yy");
                    //text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,5) >= @START_DATE AND CONVERT(nvarchar(11),create_date,5) <= @END_DATE) ";
                    text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,103) >= @START_DATE AND CONVERT(nvarchar(11),create_date,103) <= @END_DATE) ORDER BY create_date DESC ";
                }
                else if(!string.IsNullOrEmpty(errorCode))
                {
                    text = "SELECT * FROM Action_Log WHERE error_code LIKE '%" + errorCode + "%' ORDER BY create_date DESC ";
                }
                else
                {
                    text = "SELECT * FROM Action_Log ORDER BY create_date DESC ";
                }
                
                
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.Connection = conn;
                sqlCeCommand.CommandType = System.Data.CommandType.Text;

                if (!string.IsNullOrEmpty(startDate))
                    sqlCeCommand.Parameters.Add("@START_DATE", SqlDbType.NVarChar).Value = startDate;
                if (!string.IsNullOrEmpty(endDate))
                    sqlCeCommand.Parameters.Add("@END_DATE", SqlDbType.NVarChar).Value = endDate;

                sqlCeCommand.CommandText = text.ToString();
                SqlCeDataReader dr = sqlCeCommand.ExecuteReader();
                dataTable.Load(dr);
                conn.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public System.Data.DataTable getPDALoggingBycriteria(string startDate, string endDate, string errorCode)
        {
            System.Data.DataTable result;
            try
            {
                string _startDate = string.Empty;
                string _endDate = string.Empty;
                //string connectionString_temp = @"Data Source=C:\Data Base SDF\TJG.sdf;Default Lock Timeout = 30000;Max Database Size = 2560;Password=pass;Persist Security Info=True";
                //string connectionString = string.IsNullOrEmpty(ConfigurationManager.AppSettings["conn"].ToString()) ? connectionString_temp : ConfigurationManager.AppSettings["conn"].ToString().Trim();
                SqlCeConnection conn = new SqlCeConnection(connectionString);
                conn.Open();
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = string.Empty;
                if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) && !string.IsNullOrEmpty(errorCode))
                {
                    //_startDate = startDate.Value.ToString("dd-MM-yy");
                    //_endDate = endDate.Value.ToString("dd-MM-yy");
                    //text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,5) >= @START_DATE AND CONVERT(nvarchar(11),create_date,5) <= @END_DATE) AND error_code LIKE '%"+ errorCode +"%' ";
                    text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,103) >= @START_DATE AND CONVERT(nvarchar(11),create_date,103) <= @END_DATE) AND error_code LIKE '%" + errorCode + "%' ORDER BY create_date DESC ";
                }
                else if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    //_startDate = startDate.Value.ToString("dd-MM-yy");
                    //_endDate = endDate.Value.ToString("dd-MM-yy");
                    //text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,5) >= @START_DATE AND CONVERT(nvarchar(11),create_date,5) <= @END_DATE) ";
                    text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,103) >= @START_DATE AND CONVERT(nvarchar(11),create_date,103) <= @END_DATE) ORDER BY create_date DESC ";
                }
                else if (!string.IsNullOrEmpty(errorCode))
                {
                    text = "SELECT * FROM Action_Log WHERE error_code LIKE '%" + errorCode + "%' ORDER BY create_date DESC ";
                }
                else
                {
                    text = "SELECT * FROM Action_Log ORDER BY create_date DESC ";
                }


                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.Connection = conn;
                sqlCeCommand.CommandType = System.Data.CommandType.Text;

                if (!string.IsNullOrEmpty(startDate))
                    sqlCeCommand.Parameters.Add("@START_DATE", SqlDbType.NVarChar).Value = startDate;
                if (!string.IsNullOrEmpty(endDate))
                    sqlCeCommand.Parameters.Add("@END_DATE", SqlDbType.NVarChar).Value = endDate;

                sqlCeCommand.CommandText = text.ToString();
                SqlCeDataReader dr = sqlCeCommand.ExecuteReader();
                dataTable.Load(dr);
                conn.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region 5. Insert Log

        public bool Insert_Log(string error_code, string error_message, string page_form, string method_name)
        {
            bool _result = false;
            SqlCeConnection conn = new SqlCeConnection(connectionStringLogging);
            conn.Open();
            try
            {
                
                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO Action_Log (id, error_code, error_message, page_form, method_name, create_date)  ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}', '{3}', '{4}', getdate()) ", Guid.NewGuid().ToString().ToUpper().Trim(), error_code, error_message.Replace("'", ""), page_form, method_name));

                SqlCeCommand command = new SqlCeCommand();
                command.Connection = conn;
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                if (command.ExecuteNonQuery() > 0)
                    _result = true;
                conn.Close();
            }
            catch (SqlCeException)
            {
                _result = false;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
            return _result;
        }

        #endregion

        #region Dispose
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}