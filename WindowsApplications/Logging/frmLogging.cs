﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using ExportDatas.ExportData;
using WindowsApplications.EventControls;

namespace WindowsApplications.Logging
{
    public partial class frmLogging : Form
    {
        #region Member

        DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

        #endregion

        #region Constructor

        public frmLogging()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        private bool ValidateSearch(DateTime? start, DateTime? end)
        {
            if (start.HasValue && end.HasValue)
            {
                if (start.Value.Date > end.Value)
                {
                    MsgBox.ClassMsgBox.ClassMsg.DialogError("End date should be greater than Start date");
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Event

        private void frmLogging_Load(object sender, EventArgs e)
        {
            //--GridView
            this.dgvPCLogging.TopLeftHeaderCell.Value = "ID";
            this.dgvPCLogging.AutoGenerateColumns = false;
            DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvPCLogging
			    });
            this.dgvPDALogging.TopLeftHeaderCell.Value = "ID";
            this.dgvPDALogging.AutoGenerateColumns = false;
            DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvPDALogging
			    });
            this.txtErrorCode.Focus();
            this.txtErrorCode.SelectAll();
        }

        // Tab PC
        private void btnPCSearch_Click(object sender, EventArgs e)
        {
            DateTime? start;
            DateTime? end;

            if (!dtpPCStartDate.Checked)
                start = null;
            else
                start = dtpPCStartDate.Value;

            if (!dtpPCToDate.Checked)
                end = null;
            else
                end = dtpPCToDate.Value;

            if (!ValidateSearch(start, end))
                return;

            using (Execute.Execution _execute = new Execute.Execution())
            {
                string StartDate = dtpPCStartDate.Checked == false ? string.Empty : dtpPCStartDate.Value.ToString("dd/MM/yyyy", usDtfi).Trim();
                string EndDate = dtpPCToDate.Checked == false ? string.Empty : dtpPCToDate.Value.ToString("dd/MM/yyyy", usDtfi).Trim();
                DataTable dtSearch = _execute.getLoggingBycriteria(string.IsNullOrEmpty(StartDate) ? string.Empty : StartDate.Trim(),
                    string.IsNullOrEmpty(EndDate) ? string.Empty : EndDate.Trim(), this.txtErrorCode.Text.Trim());

                if (!dtSearch.IsNullOrNoRows())
                {
                    this.dgvPCLogging.DataSource = dtSearch;
                }
                else
                {
                    this.dgvPCLogging.DataSource = dtSearch;
                }
            }
        }
        private void btnPCClear_Click(object sender, EventArgs e)
        {
            this.dtpPCStartDate.Value = DateTime.Now;
            this.dtpPCToDate.Value = DateTime.Now;
            this.dtpPCStartDate.Checked = false;
            this.dtpPCToDate.Checked = false;
            this.txtErrorCode.Text = string.Empty;

            this.txtErrorCode.Focus();
            this.txtErrorCode.SelectAll();
        }
        // Tab PDA
        private void btnPDASearch_Click(object sender, EventArgs e)
        {
            DateTime? start;
            DateTime? end;

            if (!dtpPCStartDate.Checked)
                start = null;
            else
                start = dtpPDAStartDate.Value;

            if (!dtpPCToDate.Checked)
                end = null;
            else
                end = dtpPDAToDate.Value;

            if (!ValidateSearch(start, end))
                return;

            using (Execute.Execution _execute = new Execute.Execution())
            {
                string StartDate = dtpPCStartDate.Checked == false ? string.Empty : dtpPCStartDate.Value.ToString("dd/MM/yyyy", usDtfi).Trim();
                string EndDate = dtpPCToDate.Checked == false ? string.Empty : dtpPCToDate.Value.ToString("dd/MM/yyyy", usDtfi).Trim();
                DataTable dtSearch = _execute.getPDALoggingBycriteria(string.IsNullOrEmpty(StartDate) ? string.Empty : StartDate.Trim(),
                    string.IsNullOrEmpty(EndDate) ? string.Empty : EndDate.Trim(), this.txtPDAErrorCode.Text.Trim());

                if (!dtSearch.IsNullOrNoRows())
                {
                    this.dgvPDALogging.DataSource = dtSearch;
                }
                else
                {
                    this.dgvPDALogging.DataSource = dtSearch;
                }
            }
        }
        private void btnPDAClear_Click(object sender, EventArgs e)
        {
            this.dtpPDAStartDate.Value = DateTime.Now;
            this.dtpPDAToDate.Value = DateTime.Now;
            this.dtpPDAStartDate.Checked = false;
            this.dtpPDAToDate.Checked = false;
            this.txtPDAErrorCode.Text = string.Empty;

            this.txtPDAErrorCode.Focus();
            this.txtPDAErrorCode.SelectAll();
        }

        #endregion
    }
}