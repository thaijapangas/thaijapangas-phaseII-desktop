﻿namespace WindowsApplications.Logging
{
    partial class frmLogging
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtErrorCode = new System.Windows.Forms.TextBox();
            this.dtpPCToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpPCStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblErrorCode = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.btnPCClear = new System.Windows.Forms.Button();
            this.btnPCSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlTireSize = new System.Windows.Forms.Panel();
            this.dgvPCLogging = new System.Windows.Forms.DataGridView();
            this.cPCErrorCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPCErrorMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPCPageForm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPCMethodName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvPDALogging = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPDACreateDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPDAErrorCode = new System.Windows.Forms.TextBox();
            this.dtpPDAToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpPDAStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblPDAErrorCode = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPDAClear = new System.Windows.Forms.Button();
            this.btnPDASearch = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvFrom = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn6 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn5 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_from = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_guid_from = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn4 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn3 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pnlTireSize.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPCLogging)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPDALogging)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFrom)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1150, 561);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtErrorCode);
            this.tabPage1.Controls.Add(this.dtpPCToDate);
            this.tabPage1.Controls.Add(this.dtpPCStartDate);
            this.tabPage1.Controls.Add(this.lblErrorCode);
            this.tabPage1.Controls.Add(this.lblTo);
            this.tabPage1.Controls.Add(this.lblStartDate);
            this.tabPage1.Controls.Add(this.btnPCClear);
            this.tabPage1.Controls.Add(this.btnPCSearch);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.pnlTireSize);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1142, 535);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "1. [PC] Windows Form Logging";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtErrorCode
            // 
            this.txtErrorCode.Location = new System.Drawing.Point(117, 44);
            this.txtErrorCode.Name = "txtErrorCode";
            this.txtErrorCode.Size = new System.Drawing.Size(269, 20);
            this.txtErrorCode.TabIndex = 9;
            // 
            // dtpPCToDate
            // 
            this.dtpPCToDate.Checked = false;
            this.dtpPCToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpPCToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPCToDate.Location = new System.Drawing.Point(269, 12);
            this.dtpPCToDate.Name = "dtpPCToDate";
            this.dtpPCToDate.ShowCheckBox = true;
            this.dtpPCToDate.Size = new System.Drawing.Size(117, 20);
            this.dtpPCToDate.TabIndex = 8;
            // 
            // dtpPCStartDate
            // 
            this.dtpPCStartDate.Checked = false;
            this.dtpPCStartDate.CustomFormat = "dd/MM/yyyy";
            this.dtpPCStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPCStartDate.Location = new System.Drawing.Point(117, 12);
            this.dtpPCStartDate.Name = "dtpPCStartDate";
            this.dtpPCStartDate.ShowCheckBox = true;
            this.dtpPCStartDate.Size = new System.Drawing.Size(117, 20);
            this.dtpPCStartDate.TabIndex = 8;
            // 
            // lblErrorCode
            // 
            this.lblErrorCode.AutoSize = true;
            this.lblErrorCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblErrorCode.Location = new System.Drawing.Point(18, 43);
            this.lblErrorCode.Name = "lblErrorCode";
            this.lblErrorCode.Size = new System.Drawing.Size(94, 20);
            this.lblErrorCode.TabIndex = 7;
            this.lblErrorCode.Text = "Error Code :";
            this.lblErrorCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblTo.Location = new System.Drawing.Point(238, 12);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(27, 20);
            this.lblTo.TabIndex = 7;
            this.lblTo.Text = "To";
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblStartDate.Location = new System.Drawing.Point(21, 12);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(91, 20);
            this.lblStartDate.TabIndex = 7;
            this.lblStartDate.Text = "Start Date :";
            this.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnPCClear
            // 
            this.btnPCClear.BackColor = System.Drawing.Color.DarkOrange;
            this.btnPCClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPCClear.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnPCClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnPCClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnPCClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPCClear.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnPCClear.ForeColor = System.Drawing.Color.White;
            this.btnPCClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPCClear.Location = new System.Drawing.Point(473, 12);
            this.btnPCClear.Name = "btnPCClear";
            this.btnPCClear.Size = new System.Drawing.Size(72, 52);
            this.btnPCClear.TabIndex = 6;
            this.btnPCClear.Tag = "";
            this.btnPCClear.Text = "Clear";
            this.btnPCClear.UseVisualStyleBackColor = false;
            this.btnPCClear.Click += new System.EventHandler(this.btnPCClear_Click);
            // 
            // btnPCSearch
            // 
            this.btnPCSearch.BackColor = System.Drawing.Color.LimeGreen;
            this.btnPCSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPCSearch.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnPCSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnPCSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnPCSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPCSearch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnPCSearch.ForeColor = System.Drawing.Color.White;
            this.btnPCSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPCSearch.Location = new System.Drawing.Point(395, 12);
            this.btnPCSearch.Name = "btnPCSearch";
            this.btnPCSearch.Size = new System.Drawing.Size(72, 52);
            this.btnPCSearch.TabIndex = 6;
            this.btnPCSearch.Tag = "";
            this.btnPCSearch.Text = "Search";
            this.btnPCSearch.UseVisualStyleBackColor = false;
            this.btnPCSearch.Click += new System.EventHandler(this.btnPCSearch_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(8, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 2;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlTireSize
            // 
            this.pnlTireSize.Controls.Add(this.dgvPCLogging);
            this.pnlTireSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlTireSize.Location = new System.Drawing.Point(3, 84);
            this.pnlTireSize.Name = "pnlTireSize";
            this.pnlTireSize.Size = new System.Drawing.Size(1136, 448);
            this.pnlTireSize.TabIndex = 1;
            // 
            // dgvPCLogging
            // 
            this.dgvPCLogging.AllowUserToAddRows = false;
            this.dgvPCLogging.AllowUserToDeleteRows = false;
            this.dgvPCLogging.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPCLogging.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cPCErrorCode,
            this.cPCErrorMessage,
            this.cPCPageForm,
            this.cPCMethodName,
            this.Column5});
            this.dgvPCLogging.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPCLogging.Location = new System.Drawing.Point(0, 0);
            this.dgvPCLogging.Name = "dgvPCLogging";
            this.dgvPCLogging.ReadOnly = true;
            this.dgvPCLogging.Size = new System.Drawing.Size(1136, 448);
            this.dgvPCLogging.TabIndex = 0;
            // 
            // cPCErrorCode
            // 
            this.cPCErrorCode.DataPropertyName = "error_code";
            this.cPCErrorCode.HeaderText = "Error Code";
            this.cPCErrorCode.Name = "cPCErrorCode";
            this.cPCErrorCode.ReadOnly = true;
            this.cPCErrorCode.Width = 180;
            // 
            // cPCErrorMessage
            // 
            this.cPCErrorMessage.DataPropertyName = "error_message";
            this.cPCErrorMessage.HeaderText = "Error Message";
            this.cPCErrorMessage.Name = "cPCErrorMessage";
            this.cPCErrorMessage.ReadOnly = true;
            this.cPCErrorMessage.Width = 500;
            // 
            // cPCPageForm
            // 
            this.cPCPageForm.DataPropertyName = "page_form";
            this.cPCPageForm.HeaderText = "Page Form";
            this.cPCPageForm.Name = "cPCPageForm";
            this.cPCPageForm.ReadOnly = true;
            // 
            // cPCMethodName
            // 
            this.cPCMethodName.DataPropertyName = "method_name";
            this.cPCMethodName.HeaderText = "Method Name";
            this.cPCMethodName.Name = "cPCMethodName";
            this.cPCMethodName.ReadOnly = true;
            this.cPCMethodName.Width = 140;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "create_date";
            dataGridViewCellStyle1.Format = "dd/MM/yyyy HH:mm:ss";
            this.Column5.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column5.HeaderText = "Create Date";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 140;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel1);
            this.tabPage5.Controls.Add(this.txtPDAErrorCode);
            this.tabPage5.Controls.Add(this.dtpPDAToDate);
            this.tabPage5.Controls.Add(this.dtpPDAStartDate);
            this.tabPage5.Controls.Add(this.lblPDAErrorCode);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Controls.Add(this.btnPDAClear);
            this.tabPage5.Controls.Add(this.btnPDASearch);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1142, 535);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "2. [PDA] Windows Mobile Logging";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvPDALogging);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1136, 448);
            this.panel1.TabIndex = 19;
            // 
            // dgvPDALogging
            // 
            this.dgvPDALogging.AllowUserToAddRows = false;
            this.dgvPDALogging.AllowUserToDeleteRows = false;
            this.dgvPDALogging.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPDALogging.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn14,
            this.cPDACreateDate});
            this.dgvPDALogging.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPDALogging.Location = new System.Drawing.Point(0, 0);
            this.dgvPDALogging.Name = "dgvPDALogging";
            this.dgvPDALogging.ReadOnly = true;
            this.dgvPDALogging.Size = new System.Drawing.Size(1136, 448);
            this.dgvPDALogging.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "error_code";
            this.dataGridViewTextBoxColumn2.HeaderText = "Error Code";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 180;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "error_message";
            this.dataGridViewTextBoxColumn8.HeaderText = "Error Message";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 500;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "page_form";
            this.dataGridViewTextBoxColumn11.HeaderText = "Page Form";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "method_name";
            this.dataGridViewTextBoxColumn14.HeaderText = "Method Name";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 140;
            // 
            // cPDACreateDate
            // 
            this.cPDACreateDate.DataPropertyName = "create_date";
            dataGridViewCellStyle2.Format = "dd/MM/yyyy HH:mm:ss";
            this.cPDACreateDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.cPDACreateDate.HeaderText = "Create Date";
            this.cPDACreateDate.Name = "cPDACreateDate";
            this.cPDACreateDate.ReadOnly = true;
            this.cPDACreateDate.Width = 140;
            // 
            // txtPDAErrorCode
            // 
            this.txtPDAErrorCode.Location = new System.Drawing.Point(117, 44);
            this.txtPDAErrorCode.Name = "txtPDAErrorCode";
            this.txtPDAErrorCode.Size = new System.Drawing.Size(269, 20);
            this.txtPDAErrorCode.TabIndex = 18;
            // 
            // dtpPDAToDate
            // 
            this.dtpPDAToDate.Checked = false;
            this.dtpPDAToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpPDAToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPDAToDate.Location = new System.Drawing.Point(269, 12);
            this.dtpPDAToDate.Name = "dtpPDAToDate";
            this.dtpPDAToDate.ShowCheckBox = true;
            this.dtpPDAToDate.Size = new System.Drawing.Size(117, 20);
            this.dtpPDAToDate.TabIndex = 16;
            // 
            // dtpPDAStartDate
            // 
            this.dtpPDAStartDate.Checked = false;
            this.dtpPDAStartDate.CustomFormat = "dd/MM/yyyy";
            this.dtpPDAStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPDAStartDate.Location = new System.Drawing.Point(117, 12);
            this.dtpPDAStartDate.Name = "dtpPDAStartDate";
            this.dtpPDAStartDate.ShowCheckBox = true;
            this.dtpPDAStartDate.Size = new System.Drawing.Size(117, 20);
            this.dtpPDAStartDate.TabIndex = 17;
            // 
            // lblPDAErrorCode
            // 
            this.lblPDAErrorCode.AutoSize = true;
            this.lblPDAErrorCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblPDAErrorCode.Location = new System.Drawing.Point(18, 43);
            this.lblPDAErrorCode.Name = "lblPDAErrorCode";
            this.lblPDAErrorCode.Size = new System.Drawing.Size(94, 20);
            this.lblPDAErrorCode.TabIndex = 15;
            this.lblPDAErrorCode.Text = "Error Code :";
            this.lblPDAErrorCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(238, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "To";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(21, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Start Date :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnPDAClear
            // 
            this.btnPDAClear.BackColor = System.Drawing.Color.DarkOrange;
            this.btnPDAClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPDAClear.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnPDAClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnPDAClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnPDAClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPDAClear.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnPDAClear.ForeColor = System.Drawing.Color.White;
            this.btnPDAClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPDAClear.Location = new System.Drawing.Point(473, 12);
            this.btnPDAClear.Name = "btnPDAClear";
            this.btnPDAClear.Size = new System.Drawing.Size(72, 52);
            this.btnPDAClear.TabIndex = 11;
            this.btnPDAClear.Tag = "";
            this.btnPDAClear.Text = "Clear";
            this.btnPDAClear.UseVisualStyleBackColor = false;
            this.btnPDAClear.Click += new System.EventHandler(this.btnPDAClear_Click);
            // 
            // btnPDASearch
            // 
            this.btnPDASearch.BackColor = System.Drawing.Color.LimeGreen;
            this.btnPDASearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPDASearch.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnPDASearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnPDASearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnPDASearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPDASearch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnPDASearch.ForeColor = System.Drawing.Color.White;
            this.btnPDASearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPDASearch.Location = new System.Drawing.Point(395, 12);
            this.btnPDASearch.Name = "btnPDASearch";
            this.btnPDASearch.Size = new System.Drawing.Size(72, 52);
            this.btnPDASearch.TabIndex = 12;
            this.btnPDASearch.Tag = "";
            this.btnPDASearch.Text = "Search";
            this.btnPDASearch.UseVisualStyleBackColor = false;
            this.btnPDASearch.Click += new System.EventHandler(this.btnPDASearch_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(5, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 10;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvFrom
            // 
            this.dgvFrom.AllowUserToAddRows = false;
            this.dgvFrom.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFrom.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFrom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFrom.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFrom.Location = new System.Drawing.Point(0, 0);
            this.dgvFrom.Name = "dgvFrom";
            this.dgvFrom.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFrom.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvFrom.Size = new System.Drawing.Size(878, 311);
            this.dgvFrom.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "guid";
            this.dataGridViewTextBoxColumn1.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn3.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn4.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 140;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "froms";
            dataGridViewCellStyle6.Format = "dd/MM/yyyy HH:mm:ss";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn5.HeaderText = "From";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 120;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn6.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 120;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn7.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            this.dataGridViewTextBoxColumn7.Width = 300;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn9.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            this.dataGridViewTextBoxColumn9.Width = 140;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "guid";
            dataGridViewCellStyle7.Format = "dd/MM/yyyy HH:mm:ss";
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn10.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            this.dataGridViewTextBoxColumn10.Width = 120;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "guid";
            this.dataGridViewTextBoxColumn13.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn12.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn15.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "guid";
            this.dataGridViewTextBoxColumn16.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Visible = false;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "tire_size";
            this.dataGridViewTextBoxColumn17.HeaderText = "Tire Size";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Visible = false;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "brand_code";
            this.dataGridViewTextBoxColumn21.HeaderText = "Brand Code";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "guid";
            this.dataGridViewTextBoxColumn20.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Visible = false;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn19.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Visible = false;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn18.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Visible = false;
            this.dataGridViewTextBoxColumn18.Width = 130;
            // 
            // dataGridViewButtonColumn6
            // 
            this.dataGridViewButtonColumn6.DataPropertyName = "Delete";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.NullValue = "Delete";
            this.dataGridViewButtonColumn6.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewButtonColumn6.HeaderText = "";
            this.dataGridViewButtonColumn6.Name = "dataGridViewButtonColumn6";
            this.dataGridViewButtonColumn6.ReadOnly = true;
            this.dataGridViewButtonColumn6.Text = "Delete";
            this.dataGridViewButtonColumn6.Width = 70;
            // 
            // dataGridViewButtonColumn5
            // 
            this.dataGridViewButtonColumn5.DataPropertyName = "Edit";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.NullValue = "Edit";
            this.dataGridViewButtonColumn5.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewButtonColumn5.HeaderText = "";
            this.dataGridViewButtonColumn5.Name = "dataGridViewButtonColumn5";
            this.dataGridViewButtonColumn5.ReadOnly = true;
            this.dataGridViewButtonColumn5.Text = "Edit";
            this.dataGridViewButtonColumn5.Width = 70;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "brand_name";
            this.dataGridViewTextBoxColumn22.HeaderText = "Brand Name";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Visible = false;
            // 
            // c_from
            // 
            this.c_from.DataPropertyName = "froms";
            this.c_from.HeaderText = "From";
            this.c_from.Name = "c_from";
            this.c_from.ReadOnly = true;
            // 
            // c_guid_from
            // 
            this.c_guid_from.DataPropertyName = "guid";
            this.c_guid_from.HeaderText = "Column1";
            this.c_guid_from.Name = "c_guid_from";
            this.c_guid_from.ReadOnly = true;
            this.c_guid_from.Visible = false;
            // 
            // dataGridViewButtonColumn4
            // 
            this.dataGridViewButtonColumn4.DataPropertyName = "Delete";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.NullValue = "Delete";
            this.dataGridViewButtonColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewButtonColumn4.HeaderText = "";
            this.dataGridViewButtonColumn4.Name = "dataGridViewButtonColumn4";
            this.dataGridViewButtonColumn4.ReadOnly = true;
            this.dataGridViewButtonColumn4.Text = "Delete";
            this.dataGridViewButtonColumn4.Width = 70;
            // 
            // dataGridViewButtonColumn3
            // 
            this.dataGridViewButtonColumn3.DataPropertyName = "Edit";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.NullValue = "Edit";
            this.dataGridViewButtonColumn3.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewButtonColumn3.HeaderText = "";
            this.dataGridViewButtonColumn3.Name = "dataGridViewButtonColumn3";
            this.dataGridViewButtonColumn3.ReadOnly = true;
            this.dataGridViewButtonColumn3.Text = "Edit";
            this.dataGridViewButtonColumn3.Width = 70;
            // 
            // frmLogging
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 561);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogging";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View Logging";
            this.Load += new System.EventHandler(this.frmLogging_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.pnlTireSize.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPCLogging)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPDALogging)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFrom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lblErrorCode;
        private System.Windows.Forms.Label lblStartDate;
        protected System.Windows.Forms.Button btnPCSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn6;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridView dgvFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_from;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_guid_from;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn4;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn3;
        private System.Windows.Forms.TextBox txtErrorCode;
        private System.Windows.Forms.DateTimePicker dtpPCToDate;
        private System.Windows.Forms.DateTimePicker dtpPCStartDate;
        private System.Windows.Forms.Label lblTo;
        protected System.Windows.Forms.Button btnPCClear;
        private System.Windows.Forms.TextBox txtPDAErrorCode;
        private System.Windows.Forms.DateTimePicker dtpPDAToDate;
        private System.Windows.Forms.DateTimePicker dtpPDAStartDate;
        private System.Windows.Forms.Label lblPDAErrorCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        protected System.Windows.Forms.Button btnPDAClear;
        protected System.Windows.Forms.Button btnPDASearch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnlTireSize;
        private System.Windows.Forms.DataGridView dgvPCLogging;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvPDALogging;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPCErrorCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPCErrorMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPCPageForm;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPCMethodName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPDACreateDate;
    }
}