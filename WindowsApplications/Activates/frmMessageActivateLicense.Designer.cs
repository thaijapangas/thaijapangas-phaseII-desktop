﻿namespace WindowsApplications.Activates
{
    partial class frmMessageActivateLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitleWarning = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblFooterWarning = new System.Windows.Forms.Label();
            this.lblExpire = new System.Windows.Forms.Label();
            this.lblExpireFooter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitleWarning
            // 
            this.lblTitleWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblTitleWarning.ForeColor = System.Drawing.Color.Red;
            this.lblTitleWarning.Location = new System.Drawing.Point(178, 7);
            this.lblTitleWarning.Name = "lblTitleWarning";
            this.lblTitleWarning.Size = new System.Drawing.Size(272, 24);
            this.lblTitleWarning.TabIndex = 0;
            this.lblTitleWarning.Text = "License key can be used to";
            // 
            // lblDate
            // 
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblDate.ForeColor = System.Drawing.Color.Red;
            this.lblDate.Location = new System.Drawing.Point(205, 48);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(219, 23);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "label1";
            // 
            // lblFooterWarning
            // 
            this.lblFooterWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblFooterWarning.ForeColor = System.Drawing.Color.Red;
            this.lblFooterWarning.Location = new System.Drawing.Point(8, 95);
            this.lblFooterWarning.Name = "lblFooterWarning";
            this.lblFooterWarning.Size = new System.Drawing.Size(612, 25);
            this.lblFooterWarning.TabIndex = 0;
            this.lblFooterWarning.Text = "Licences key to register the application on the PDA -> Menu \'Activate Licences\'";
            // 
            // lblExpire
            // 
            this.lblExpire.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblExpire.ForeColor = System.Drawing.Color.Red;
            this.lblExpire.Location = new System.Drawing.Point(85, 33);
            this.lblExpire.Name = "lblExpire";
            this.lblExpire.Size = new System.Drawing.Size(458, 25);
            this.lblExpire.TabIndex = 1;
            this.lblExpire.Text = "The program expire Please register Licences key";
            // 
            // lblExpireFooter
            // 
            this.lblExpireFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblExpireFooter.ForeColor = System.Drawing.Color.Red;
            this.lblExpireFooter.Location = new System.Drawing.Point(129, 66);
            this.lblExpireFooter.Name = "lblExpireFooter";
            this.lblExpireFooter.Size = new System.Drawing.Size(371, 23);
            this.lblExpireFooter.TabIndex = 2;
            this.lblExpireFooter.Text = "The program on the PDA -> Menu \'Activate Licences\'";
            // 
            // frmMessageActivateLicense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(628, 143);
            this.Controls.Add(this.lblExpireFooter);
            this.Controls.Add(this.lblExpire);
            this.Controls.Add(this.lblFooterWarning);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblTitleWarning);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMessageActivateLicense";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Licences key Message";
            this.Load += new System.EventHandler(this.frmMessageActivateLicense_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitleWarning;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblFooterWarning;
        private System.Windows.Forms.Label lblExpire;
        private System.Windows.Forms.Label lblExpireFooter;
    }
}