﻿namespace WindowsApplications.Activates
{
    partial class frmActivateLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmActivateLicense));
            this.txtActivateKeys = new System.Windows.Forms.TextBox();
            this.lblActivateKeys = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.picSeePassword = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picSeePassword)).BeginInit();
            this.SuspendLayout();
            // 
            // txtActivateKeys
            // 
            this.txtActivateKeys.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtActivateKeys.Location = new System.Drawing.Point(141, 22);
            this.txtActivateKeys.Name = "txtActivateKeys";
            this.txtActivateKeys.PasswordChar = '*';
            this.txtActivateKeys.Size = new System.Drawing.Size(261, 26);
            this.txtActivateKeys.TabIndex = 3;
            this.txtActivateKeys.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtActivateKeys_KeyUp);
            // 
            // lblActivateKeys
            // 
            this.lblActivateKeys.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblActivateKeys.Location = new System.Drawing.Point(19, 22);
            this.lblActivateKeys.Name = "lblActivateKeys";
            this.lblActivateKeys.Size = new System.Drawing.Size(116, 26);
            this.lblActivateKeys.TabIndex = 4;
            this.lblActivateKeys.Text = "Activating Key :";
            this.lblActivateKeys.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.LimeGreen;
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSubmit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSubmit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnSubmit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSubmit.Location = new System.Drawing.Point(313, 59);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(89, 32);
            this.btnSubmit.TabIndex = 7;
            this.btnSubmit.Tag = "";
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(19, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(383, 41);
            this.label1.TabIndex = 8;
            this.label1.Text = "Activating key has been for unlock software provide in the activate / Reactivate " +
    "form";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // picSeePassword
            // 
            this.picSeePassword.Image = global::WindowsApplications.Properties.Resources.see_password;
            this.picSeePassword.Location = new System.Drawing.Point(376, 24);
            this.picSeePassword.Name = "picSeePassword";
            this.picSeePassword.Size = new System.Drawing.Size(24, 22);
            this.picSeePassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSeePassword.TabIndex = 9;
            this.picSeePassword.TabStop = false;
            this.picSeePassword.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picSeePassword_MouseDown);
            this.picSeePassword.MouseLeave += new System.EventHandler(this.picSeePassword_MouseLeave);
            this.picSeePassword.MouseHover += new System.EventHandler(this.picSeePassword_MouseHover);
            this.picSeePassword.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picSeePassword_MouseUp);
            // 
            // frmActivateLicense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(425, 162);
            this.Controls.Add(this.picSeePassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtActivateKeys);
            this.Controls.Add(this.lblActivateKeys);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmActivateLicense";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Activate License";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmActivateLicense_FormClosed);
            this.Load += new System.EventHandler(this.frmActivateLicense_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picSeePassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtActivateKeys;
        private System.Windows.Forms.Label lblActivateKeys;
        protected System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picSeePassword;
    }
}