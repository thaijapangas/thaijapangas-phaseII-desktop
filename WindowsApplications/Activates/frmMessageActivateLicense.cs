﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsApplications.Activates
{
    public partial class frmMessageActivateLicense : Form
    {
        #region Member

        string _expire_type = string.Empty;
        string _values = string.Empty;
        
        #endregion

        #region Constructor

        public frmMessageActivateLicense(string expire_type, string values)
        {
            InitializeComponent();
            _expire_type = expire_type.Trim();
            _values = values.Trim();
        }

        #endregion

        #region Event

        private void frmMessageActivateLicense_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_expire_type))
            {
                if (_expire_type.Trim() == "WARNING")
                {
                    this.lblDate.Text = _values;
                    this.lblTitleWarning.Visible = true;
                    this.lblDate.Visible = true;
                    this.lblFooterWarning.Visible = true;

                    this.lblExpire.Visible = false;
                    this.lblExpireFooter.Visible = false;
                }
                else if (_expire_type.Trim() == "EXPIRE_COMPLATE")
                {
                    this.lblTitleWarning.Visible = false;
                    this.lblDate.Visible = false;
                    this.lblFooterWarning.Visible = false;

                    this.lblExpire.Visible = true;
                    this.lblExpireFooter.Visible = true;
                }
            }
        }

        #endregion
    }
}
