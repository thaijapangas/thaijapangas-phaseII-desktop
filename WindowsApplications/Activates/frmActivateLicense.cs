﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MsgBox.ClassMsgBox;
using WindowsApplications.EventControls;
using System.Globalization;
using WindowsApplications.Execute;

namespace WindowsApplications.Activates
{
    public partial class frmActivateLicense : Form
    {
        #region Constructor

        public frmActivateLicense(string _action_form)
        {
            InitializeComponent();
            Action_form = _action_form.Trim();
        }

        #endregion

        #region Member

        string Action_form = string.Empty;
        DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
        public string AppExit { get; set; }

        #endregion

        #region Event

        private void frmActivateLicense_Load(object sender, EventArgs e)
        {
            this.txtActivateKeys.Focus();
            this.txtActivateKeys.SelectAll();
        }
        private void frmActivateLicense_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.Action_form.Trim() == "ACTIVATES")
            {
                this.Close();
            }
            else
            {
                AppExit = "EXIT";
                Application.Exit();
            }
        }

        private void txtActivateKeys_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSubmit.PerformClick();
            }
        }
        private void picSeePassword_MouseDown(object sender, MouseEventArgs e)
        {
            this.txtActivateKeys.PasswordChar = '\0';
        }
        private void picSeePassword_MouseUp(object sender, MouseEventArgs e)
        {
            this.txtActivateKeys.PasswordChar = '*';
        }
        private void picSeePassword_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }
        private void picSeePassword_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtActivateKeys.Text.Trim()))
            {
                MsgBox.ClassMsgBox.ClassMsg.DialogWarning("กรุณากรอกหมายเลขลงทะเบียนโปรแกรม");
                this.txtActivateKeys.Focus();
                this.txtActivateKeys.SelectAll();
                return;
            }
            else if(!string.IsNullOrEmpty(this.txtActivateKeys.Text.Trim()))
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                        string _crrent_date = Convert.ToDateTime(Executing.Instance.GetDateServer()).ToString("yyyyMMdd", usDtfi);
                        string _licence_key = Convert.ToString((Convert.ToInt64(this.txtActivateKeys.Text.Trim()) - 39335472));
                        if (!string.IsNullOrEmpty(_licence_key))
                        {
                            if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "TRUE")
                            {
                                ClassMsg.DialogInfomation("หมายเลขลงทะเบียนถูกต้อง กรุณาเปิดโปรแกรมใหม่อีกครั้ง");
                                Application.Exit();
                            }
                            else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()).IndexOf("|") >= 0)
                            {
                                string[] array = Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()).Split('|');
                                ClassMsg.DialogInfomation("หมายเลขลงทะเบียนถูกต้อง จะมีอายุการใช้งานได้ถึงวันที่ " + array[1].ToString().Trim() + " กรุณาเปิดโปรแกรมใหม่อีกครั้ง");
                                Application.Exit();
                            }
                            else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "TRUE_EXPIRED")
                            {
                                if (MessageActivate.DialogQuestion("หมายเลขทะเบียนนี้หมดอายุแล้ว คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                                                == System.Windows.Forms.DialogResult.Yes)
                                {
                                    this.txtActivateKeys.Focus();
                                    this.txtActivateKeys.SelectAll();
                                    return;
                                }
                                else
                                {
                                    if (this.Action_form.Trim() == "ACTIVATES")
                                    {
                                        this.Close();
                                    }
                                    else
                                    {
                                        Application.Exit();
                                    }
                                }
                            }
                            else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "FALSE")
                            {
                                if (MessageActivate.DialogQuestion("หมายเลขลงทะเบียนไม่ถูกต้อง คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                                                == System.Windows.Forms.DialogResult.Yes)
                                {
                                    this.txtActivateKeys.Focus();
                                    this.txtActivateKeys.SelectAll();
                                    return;
                                }
                                else
                                {
                                    if (this.Action_form.Trim() == "ACTIVATES")
                                    {
                                        this.Close();
                                    }
                                    else
                                    {
                                        Application.Exit();
                                    }
                                }
                            }
                        }
                }
                catch (Exception)
                {
                    if (MessageActivate.DialogQuestion("หมายเลขลงทะเบียนไม่ถูกต้อง คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                            == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.txtActivateKeys.Focus();
                        this.txtActivateKeys.SelectAll();
                        return;
                    }
                    else
                    {
                        if (this.Action_form.Trim() == "ACTIVATES")
                        {
                            this.Close();
                        }
                        else
                        {
                            Application.Exit();
                        }
                    }
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        #endregion
    }
}