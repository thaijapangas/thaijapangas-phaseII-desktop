﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.IO;


namespace WindowsApplications.EventControls
{
	/// <summary>
	/// คลาสจัดการ Datagrid
	/// </summary>
	public class DataGridControl
	{
		/// <summary>
		/// แสดงเลขลำดับรายการ ใน data grid
		/// </summary>
		/// <param name="datagridSource"></param>
		public static void PaintNumberRowOnDatagrid(params  System.Windows.Forms.DataGridView[] datagridSource)
		{
			foreach (System.Windows.Forms.DataGridView dgv in datagridSource)
			{
				dgv.RowPostPaint -= new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(PaintNumberRowOnDatagridHandler);
				dgv.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(PaintNumberRowOnDatagridHandler);
			}
		}
		private static void PaintNumberRowOnDatagridHandler(object sender, System.Windows.Forms.DataGridViewRowPostPaintEventArgs e)
		{
			SolidBrush b = new SolidBrush(((System.Windows.Forms.DataGridView)sender).RowHeadersDefaultCellStyle.ForeColor);
			e.Graphics.DrawString((e.RowIndex + 1).ToString(), ((System.Windows.Forms.DataGridView)sender).DefaultCellStyle.Font, b,
			e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 4);
		}
		/// <summary>
		/// SetColumn Datagird
		/// </summary>
		/// <param name="dgvData"></param>
		public static void SetGridLastColumnFill(ref DataGridView dgvData)
		{
			for (int iColIndex = (dgvData.ColumnCount - 1); iColIndex >= 0; iColIndex--)
			{
				if (dgvData.Columns[iColIndex].Visible == true && dgvData.Columns[iColIndex].CellType == typeof(DataGridViewTextBoxCell))
				{
					//Set Last Column Size Fill
					dgvData.Columns[iColIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
					iColIndex = -1;
				}
			}
		}
	}

    public class DGVColumnHeader : DataGridViewColumnHeaderCell
    {
        private Rectangle CheckBoxRegion;
        private bool checkAll = false;

        protected override void Paint(Graphics graphics,
            Rectangle clipBounds, Rectangle cellBounds, int rowIndex,
            DataGridViewElementStates dataGridViewElementState,
            object value, object formattedValue, string errorText,
            DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {

            base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value,
                formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);

            graphics.FillRectangle(new SolidBrush(cellStyle.BackColor), cellBounds);

            CheckBoxRegion = new Rectangle(
                cellBounds.Location.X + 1,
                cellBounds.Location.Y + 2,
                25, cellBounds.Size.Height - 4);


            if (this.checkAll)
                ControlPaint.DrawCheckBox(graphics, CheckBoxRegion, ButtonState.Checked);
            else
                ControlPaint.DrawCheckBox(graphics, CheckBoxRegion, ButtonState.Normal);

            Rectangle normalRegion =
                new Rectangle(
                cellBounds.Location.X + 1 + 25,
                cellBounds.Location.Y,
                cellBounds.Size.Width - 26,
                cellBounds.Size.Height);

            graphics.DrawString(value.ToString(), cellStyle.Font, new SolidBrush(cellStyle.ForeColor), normalRegion);
        }

        protected override void OnMouseClick(DataGridViewCellMouseEventArgs e)
        {
            //Convert the CheckBoxRegion 
            Rectangle rec = new Rectangle(new Point(0, 0), this.CheckBoxRegion.Size);
            this.checkAll = !this.checkAll;
            if (rec.Contains(e.Location))
            {
                this.DataGridView.Invalidate();
            }
            base.OnMouseClick(e);
        }

        public bool CheckAll
        {
            get { return this.checkAll; }
            set { this.checkAll = value; }
        }
    }

    public static class EventControls
    {
        #region DataTable

        public static bool IsNullOrNoRows(this DataTable dt)
        {
            bool result = true;

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool HasData(this DataSet instance)
        {
            if (instance == null)
                return false;

            foreach (DataTable table in instance.Tables)
            {
                if (table.Rows.Count > 0)
                    return true;
            }

            return false;
        }
        public static byte[] ConvertToByteArray(this DataSet instance)
        {
            if (instance == null)
                return null;

            byte[] dataBytes = null;
            long formatFlag = 0;    //no binary data
            long datasetPartLength = 0;

            using (MemoryStream ms = new MemoryStream())
            {
                instance.WriteXml(ms, XmlWriteMode.WriteSchema);

                byte[] datasetBytes = ms.ToArray();
                datasetPartLength = datasetBytes.Length;

                dataBytes = new byte[8 + datasetPartLength];

                Buffer.BlockCopy(BitConverter.GetBytes(formatFlag), 0, dataBytes, 0, 8);
                Buffer.BlockCopy(datasetBytes, 0, dataBytes, 8, (int)datasetPartLength);
            }

            return dataBytes;
        }

        #endregion
    }
}