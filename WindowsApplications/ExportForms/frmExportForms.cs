﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExportDatas.ExportData;
using MsgBox.ClassMsgBox;
using WindowsApplications.EventControls;
using WindowsApplications.CallingService;
using WindowsApplications.Execute;
using WindowsApplications.Activates;
using System.Reflection;
using System.Diagnostics;
using WindowsApplications.Logging;
using System.Configuration;

namespace WindowsApplications.ExportForms
{
    public partial class frmExportForms : Form
    {
        #region Constructor

        public frmExportForms()
        {
            InitializeComponent();

        }

        #endregion

        #region Property

        DataTable dtReceive = new DataTable();
        /// <New object RAPI.>
        /// 
        /// </summary>
        private static OpenNETCF.Desktop.Communication.RAPI rapi;

        #endregion

        #region Method

        /// <Method Connect Device.>
        /// Method Connect Device.
        /// </summary>
        private void ConnectDevice()
        {
            rapi = new OpenNETCF.Desktop.Communication.RAPI();
            try
            {
                if ((rapi.DevicePresent))
                {
                    //"Connect HandHeld Success";
                    lblResultSync.Text = "Sync HandHeld";
                    lblResultSync.ForeColor = Color.Green;
                }
                else
                {
                    //"Connect HandHeld Not Success";
                    lblResultSync.Text = "Not Sync HandHeld";
                    lblResultSync.ForeColor = Color.Red;
                }
            }
            catch (Exception)
            {
                rapi.Dispose();
            }
        }

        #endregion

        #region Event

        private void frmExportForms_Load(object sender, EventArgs e)
        {
            try
            {
                //Action Form Load.
                lblDate.Text = DateTime.Now.ToString("dd/MM/yy").Trim();
                lblTimeRun.Text = DateTime.Now.ToString("HH:mm:ss").Trim();
                // Hide tab control.
                this.tabControl1.TabPages.Remove(tabPage1);
                this.tabControl1.TabPages.Remove(tabPage2);
                this.tabControl1.TabPages.Remove(tabPage3);
                this.tabControl1.TabPages.Remove(tabPage4);

                string AppExit = string.Empty;
                string strExpireSoftware = string.IsNullOrEmpty(Executing.Instance.CheckExpireSoftware()) ? string.Empty : Executing.Instance.CheckExpireSoftware().Trim();
                if (!string.IsNullOrEmpty(strExpireSoftware))
                {
                    if (strExpireSoftware.Trim() == "TRUE")
                    {
                        this.btnActivateLicence.Visible = true;
                    }
                    else if (strExpireSoftware.Trim() == "LIFETIME")
                    {
                        this.btnActivateLicence.Visible = false;
                    }
                    else if (strExpireSoftware.Trim() == "EXPIRE")
                    {
                        if (MessageActivate.DialogQuestion("The program expire. Please contact the administrator to register the program.?")
                                == System.Windows.Forms.DialogResult.Yes)
                        {
                            using (frmActivateLicense fAvtivate = new frmActivateLicense(string.Empty))
                            {
                                fAvtivate.ShowDialog();
                                AppExit = fAvtivate.AppExit.Trim();
                            }
                        }
                        else
                        {
                            AppExit = "EXIT";
                            Application.Exit();
                        }
                    }
                    else if (strExpireSoftware.Trim() == "FALSE")
                    {
                        //MsgBox.ClassMsgBox.ClassMsg.DialogWarning("หมายเลขลงทะเบียนไม่ถูกต้องไม่สามารถเปิดโปรแกรมได้ กรุณาติดต่อ IT หรือผู้เกี่ยวข้องกับระบบ");
                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Invalid registration number can not open, contact the IT or related systems.");
                        Application.Exit();
                    }
                }

                if (!string.IsNullOrEmpty(AppExit))
                {
                    if (AppExit.Trim() != "EXIT")
                    {
                        ConnectDevice();
                        //--GridView
                        this.dgvGIRefDelivery.TopLeftHeaderCell.Value = "ID";
                        this.dgvGIRefDelivery.AutoGenerateColumns = false;
                        DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGIRefDelivery
			    });
                        this.dgvDistributionRet.TopLeftHeaderCell.Value = "ID";
                        this.dgvDistributionRet.AutoGenerateColumns = false;
                        DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvDistributionRet
			    });
                        this.dgvGIRefIOEmpty.TopLeftHeaderCell.Value = "ID";
                        this.dgvGIRefIOEmpty.AutoGenerateColumns = false;
                        DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGIRefIOEmpty
			    });
                        this.dgvGRFromProduction.TopLeftHeaderCell.Value = "ID";
                        this.dgvGRFromProduction.AutoGenerateColumns = false;
                        DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGRFromProduction
			    }); ;

                        this.lblVersion.Text = " Versions : " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
                        this.WindowState = FormWindowState.Maximized;
                    }
                }
                else
                {
                    ConnectDevice();
                    //--GridView
                    this.dgvGIRefDelivery.TopLeftHeaderCell.Value = "ID";
                    this.dgvGIRefDelivery.AutoGenerateColumns = false;
                    DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGIRefDelivery
			    });
                    this.dgvDistributionRet.TopLeftHeaderCell.Value = "ID";
                    this.dgvDistributionRet.AutoGenerateColumns = false;
                    DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvDistributionRet
			    });
                    this.dgvGIRefIOEmpty.TopLeftHeaderCell.Value = "ID";
                    this.dgvGIRefIOEmpty.AutoGenerateColumns = false;
                    DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGIRefIOEmpty
			    });
                    this.dgvGRFromProduction.TopLeftHeaderCell.Value = "ID";
                    this.dgvGRFromProduction.AutoGenerateColumns = false;
                    DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGRFromProduction
			    });

                    this.dgvWarehouse.TopLeftHeaderCell.Value = "ID";
                    this.dgvWarehouse.AutoGenerateColumns = false;
                    DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvWarehouse
			    });
                    this.dgvWarehouseCylinder.AutoGenerateColumns = false;

                    this.dgvDelivery.TopLeftHeaderCell.Value = "ID";
                    this.dgvDelivery.AutoGenerateColumns = false;
                    DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvDelivery
			    });
                    this.dgvDeliveryCylinder.AutoGenerateColumns = false;

                    this.dgvNewBarcode.TopLeftHeaderCell.Value = "ID";
                    this.dgvNewBarcode.AutoGenerateColumns = false;
                    DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvNewBarcode
			    });
                    this.dgvNewBarcodeCylinder.AutoGenerateColumns = false;

                    this.lblVersion.Text = " Versions : " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    this.WindowState = FormWindowState.Maximized;
                }
            }
            catch (Exception ex)
            {
                // Write logging
                using (Execution _execute = new Execution())
                {
                    _execute.Insert_Log("MAIN_LOAD", ex.Message.ToString().Trim(), "frmExportForms.cs", "frmExportForms_Load");
                }

                ConnectDevice();
                //--GridView
                this.dgvGIRefDelivery.TopLeftHeaderCell.Value = "ID";
                this.dgvGIRefDelivery.AutoGenerateColumns = false;
                DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGIRefDelivery
			    });
                this.dgvDistributionRet.TopLeftHeaderCell.Value = "ID";
                this.dgvDistributionRet.AutoGenerateColumns = false;
                DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvDistributionRet
			    });
                this.dgvGIRefIOEmpty.TopLeftHeaderCell.Value = "ID";
                this.dgvGIRefIOEmpty.AutoGenerateColumns = false;
                DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGIRefIOEmpty
			    });
                this.dgvGRFromProduction.TopLeftHeaderCell.Value = "ID";
                this.dgvGRFromProduction.AutoGenerateColumns = false;
                DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvGRFromProduction
			    });

                this.dgvWarehouse.TopLeftHeaderCell.Value = "ID";
                this.dgvWarehouse.AutoGenerateColumns = false;
                DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvWarehouse
			    });
                this.dgvWarehouseCylinder.AutoGenerateColumns = false;

                this.dgvDelivery.TopLeftHeaderCell.Value = "ID";
                this.dgvDelivery.AutoGenerateColumns = false;
                DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvDelivery
			    });
                this.dgvDeliveryCylinder.AutoGenerateColumns = false;

                this.dgvNewBarcode.TopLeftHeaderCell.Value = "ID";
                this.dgvNewBarcode.AutoGenerateColumns = false;
                DataGridControl.PaintNumberRowOnDatagrid(new DataGridView[]
			    {
				    this.dgvNewBarcode
			    });
                this.dgvNewBarcodeCylinder.AutoGenerateColumns = false;

                this.lblVersion.Text = " Versions : " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
                this.WindowState = FormWindowState.Maximized;
            }
        }
        private void frmExportForms_KeyDown(object sender, KeyEventArgs e)
        {
            //Event Key.
            if (e.Control && Keys.E == e.KeyCode)
            {
                this.btnExportToExcel.PerformClick();
                return;
            }
        }

        private void btnReceiveData_Click(object sender, EventArgs e)
        {
            //Receive Data From.
            Cursor.Current = Cursors.WaitCursor;
            dtReceive = null;
            //นำเข้าข้อมูล
            try
            {
                if (lblResultSync.ForeColor == Color.Red)
                {
                    //ClassMsg.DialogWarning("HandHeld ไม่ถูกเชื่อมต่อ กรุณาตรวจสอบ");
                    ClassMsg.DialogWarning("HandHeld not connected, please check.");
                    return;
                }
                else
                {
                    //if ((MessageBox.Show("คุณต้องการรับข้อมูลจาก Hand Held  ใช่หรือไม่?", "Confirm", MessageBoxButtons.YesNo,
                    if ((MessageBox.Show("Would you like to receive information from Hand Held yes or no?", "Confirm", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes))
                    {
                        rapi = new OpenNETCF.Desktop.Communication.RAPI();
                        if (rapi.DevicePresent)
                        {
                            if (!rapi.Connected)
                            {
                                //Create folder Data Base SDF in C:
                                string _create_path = @"C:\Data Base SDF\";
                                //Copy files.
                                string path = @"C:\Data Base SDF\TJG.sdf";
                                string pathPDA = "\\My Documents\\TJG.sdf";
                                //Check path in local.
                                if (!Directory.Exists(_create_path))
                                    Directory.CreateDirectory(_create_path);
                                //Class copy files from device to PC.
                                CopyFiles.Files.ClsCopyFile.CopyFromDevice(path, pathPDA);
                                rapi.Connect();
                                lblNotify.Text = "Loading...";
                                lblNotify.Refresh();
                                //Delete SDF in PDA.
                                //rapi.DeleteDeviceFile("\\My Documents\\BarcodeTS.sdf");
                                rapi.Disconnect();
                            }
                            rapi.Dispose();

                            if (Executing.Instance.getConfigSignOn("CONFIG_SIGNON") == "Y")
                            {
                                #region ไม่สนใจว่าจะ Logout หรือไม่

                                // ไม่สนใจว่าจะ Logout หรือไม่
                                string strExpireSoftware = string.IsNullOrEmpty(Executing.Instance.CheckExpireSoftware()) ? string.Empty : Executing.Instance.CheckExpireSoftware().Trim();
                                if (!string.IsNullOrEmpty(strExpireSoftware))
                                {
                                    if (strExpireSoftware.Trim() == "TRUE")
                                    {
                                        this.btnActivateLicence.Visible = true;
                                    }
                                    else if (strExpireSoftware.Trim() == "LIFETIME")
                                    {
                                        this.btnActivateLicence.Visible = false;
                                    }
                                    else if (strExpireSoftware.Trim() == "EXPIRE")
                                    {
                                        if (MessageActivate.DialogQuestion("The program expire Please contact the administrator to register the program?")
                                                == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            using (frmActivateLicense fAvtivate = new frmActivateLicense(string.Empty))
                                            {
                                                fAvtivate.ShowDialog();
                                            }
                                        }
                                        else
                                        {
                                            Application.Exit();
                                        }
                                    }
                                    else if (strExpireSoftware.Trim() == "FALSE")
                                    {
                                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Invalid registration number can not open, contact the IT or related systems.");
                                        Application.Exit();
                                    }
                                }

                                System.Data.DataTable warehouse = Executing.Instance.getWarehouse();
                                System.Data.DataTable delivery = Executing.Instance.getDelivery();
                                System.Data.DataTable new_barcode = Executing.Instance.getNewBarcode();

                                // Binding Warehouse
                                this.dgvWarehouse.DataSource = warehouse;
                                this.dgvWarehouseCylinder.DataSource = warehouse;

                                // Binding Delivery
                                this.dgvDelivery.DataSource = delivery;
                                this.dgvDeliveryCylinder.DataSource = delivery;

                                // Binding New Barcode
                                this.dgvNewBarcode.DataSource = new_barcode;
                                this.dgvNewBarcodeCylinder.DataSource = new_barcode;

                                ClassMsg.DialogInfomation("Load data from PDA completed");
                                this.lblNotify.Text = string.Empty;
                                this.Refresh();

                                Cursor.Current = Cursors.Default;

                                #endregion
                            }
                            else if (Executing.Instance.getConfigSignOn("CONFIG_SIGNON") == "N")
                            {
                                #region สนใจว่า PDA ต้อง Logout ก่อน

                                // สนใจว่า PDA ต้อง Logout ก่อน
                                if (Executing.Instance.getSignInOut() == "SIGN_OUT")
                                {
                                    #region Logout เรียบร้อยแล้ว เดิน Flow ปกติ

                                    // Logout เรียบร้อยแล้ว เดิน Flow ปกติ
                                    string strExpireSoftware = string.IsNullOrEmpty(Executing.Instance.CheckExpireSoftware()) ? string.Empty : Executing.Instance.CheckExpireSoftware().Trim();
                                    if (!string.IsNullOrEmpty(strExpireSoftware))
                                    {
                                        if (strExpireSoftware.Trim() == "TRUE")
                                        {
                                            this.btnActivateLicence.Visible = true;
                                        }
                                        else if (strExpireSoftware.Trim() == "LIFETIME")
                                        {
                                            this.btnActivateLicence.Visible = false;
                                        }
                                        else if (strExpireSoftware.Trim() == "EXPIRE")
                                        {
                                            if (MessageActivate.DialogQuestion("The program expire Please contact the administrator to register the program?")
                                                    == System.Windows.Forms.DialogResult.Yes)
                                            {
                                                using (frmActivateLicense fAvtivate = new frmActivateLicense(string.Empty))
                                                {
                                                    fAvtivate.ShowDialog();
                                                }
                                            }
                                            else
                                            {
                                                Application.Exit();
                                            }
                                        }
                                        else if (strExpireSoftware.Trim() == "FALSE")
                                        {
                                            MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Invalid registration number can not open, contact the IT or related systems.");
                                            Application.Exit();
                                        }
                                    }

                                    //Query data in sqlCe.
                                    /*
                                    System.Data.DataTable distributionRet = Executing.Instance.getDistributionRet();
                                    System.Data.DataTable gIDelivery = Executing.Instance.getGIDelivery();
                                    System.Data.DataTable gIRefIOEmpty = Executing.Instance.getGIRefIOEmpty();
                                    System.Data.DataTable gRFromProduction = Executing.Instance.getGRFromProduction();
                                    this.dgvGIRefDelivery.DataSource = gIDelivery;
                                    this.dgvDistributionRet.DataSource = distributionRet;
                                    this.dgvGIRefIOEmpty.DataSource = gIRefIOEmpty;
                                    this.dgvGRFromProduction.DataSource = gRFromProduction;
                                    */

                                    System.Data.DataTable warehouse = Executing.Instance.getWarehouse();
                                    System.Data.DataTable delivery = Executing.Instance.getDelivery();
                                    System.Data.DataTable new_barcode = Executing.Instance.getNewBarcode();

                                    // Binding Warehouse
                                    this.dgvWarehouse.DataSource = warehouse;
                                    this.dgvWarehouseCylinder.DataSource = warehouse;

                                    // Binding Delivery
                                    this.dgvDelivery.DataSource = delivery;
                                    this.dgvDeliveryCylinder.DataSource = delivery;

                                    // Binding New Barcode
                                    this.dgvNewBarcode.DataSource = new_barcode;
                                    this.dgvNewBarcodeCylinder.DataSource = new_barcode;

                                    ClassMsg.DialogInfomation("Load data from PDA completed");
                                    this.lblNotify.Text = string.Empty;
                                    this.Refresh();

                                    Cursor.Current = Cursors.Default;

                                    #endregion
                                }
                                else if (Executing.Instance.getSignInOut() == "SIGN_IN")
                                {
                                    #region ยังไม่ได้ทำการ Sign out ออกจากระบบบน PDA ไม่สามารถเดิน Flow ต่อได้

                                    // ยังไม่ได้ทำการ Sign out ออกจากระบบบน PDA ไม่สามารถเดิน Flow ต่อได้
                                    MsgBox.ClassMsgBox.ClassMsg.DialogWarning("To logout of your PDA, please Logout incorrect application on the PDA correctly.");
                                    return;

                                    #endregion
                                }

                                #endregion
                            }
                            else
                            {
                                #region ไม่สนใจว่าจะ Logout หรือไม่

                                // ไม่สนใจว่าจะ Logout หรือไม่
                                string strExpireSoftware = string.IsNullOrEmpty(Executing.Instance.CheckExpireSoftware()) ? string.Empty : Executing.Instance.CheckExpireSoftware().Trim();
                                if (!string.IsNullOrEmpty(strExpireSoftware))
                                {
                                    if (strExpireSoftware.Trim() == "TRUE")
                                    {
                                        this.btnActivateLicence.Visible = true;
                                    }
                                    else if (strExpireSoftware.Trim() == "LIFETIME")
                                    {
                                        this.btnActivateLicence.Visible = false;
                                    }
                                    else if (strExpireSoftware.Trim() == "EXPIRE")
                                    {
                                        if (MessageActivate.DialogQuestion("The program expire Please contact the administrator to register the program?")
                                                == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            using (frmActivateLicense fAvtivate = new frmActivateLicense(string.Empty))
                                            {
                                                fAvtivate.ShowDialog();
                                            }
                                        }
                                        else
                                        {
                                            Application.Exit();
                                        }
                                    }
                                    else if (strExpireSoftware.Trim() == "FALSE")
                                    {
                                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Invalid registration number can not open, contact the IT or related systems.");
                                        Application.Exit();
                                    }
                                }

                                System.Data.DataTable warehouse = Executing.Instance.getWarehouse();
                                System.Data.DataTable delivery = Executing.Instance.getDelivery();
                                System.Data.DataTable new_barcode = Executing.Instance.getNewBarcode();

                                // Binding Warehouse
                                this.dgvWarehouse.DataSource = warehouse;
                                this.dgvWarehouseCylinder.DataSource = warehouse;

                                // Binding Delivery
                                this.dgvDelivery.DataSource = delivery;
                                this.dgvDeliveryCylinder.DataSource = delivery;

                                // Binding New Barcode
                                this.dgvNewBarcode.DataSource = new_barcode;
                                this.dgvNewBarcodeCylinder.DataSource = new_barcode;

                                ClassMsg.DialogInfomation("Load data from PDA completed");
                                this.lblNotify.Text = string.Empty;
                                this.Refresh();

                                Cursor.Current = Cursors.Default;

                                #endregion
                            }
                        }
                        else
                        {
                            ClassMsg.DialogWarning("HandHeld not connected, please check");
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString().IndexOf("used by another process") > -1)
                {
                    rapi.Dispose();
                    Cursor.Current = Cursors.Default;
                    this.lblNotify.Text = "";
                    MessageBox.Show("Use Database" + System.Environment.NewLine + " Please close the program or on HandHeld " + System.Environment.NewLine + ex.Message.ToString().Trim(), "Alert");
                    //Executing.Instance.Insert_Log("PC_LoadData", "used by another process", "Class_CopyFromDevice", "btnReceiveData_Click");
                    // Write logging
                    using (Execution _execute = new Execution())
                    {
                        _execute.Insert_Log("PCLoadData", ex.Message.ToString().Trim(), "frmExportForms.cs", "btnReceiveData_Click");
                    }
                }
                else if (ex.Message.ToString().IndexOf("The type initializer") > -1)
                {
                    rapi.Dispose();
                    Cursor.Current = Cursors.Default;
                    this.lblNotify.Text = "";
                    // Write logging
                    using (Execution _execute = new Execution())
                    {
                        _execute.Insert_Log("PCLoadData", ex.Message.ToString().Trim(), "frmExportForms.cs", "btnReceiveData_Click");
                    }

                    Application.Restart();
                }
                else
                {
                    rapi.Dispose();
                    Cursor.Current = Cursors.Default;
                    this.lblNotify.Text = "";
                    MessageBox.Show("HandHeld application may be turned on." + System.Environment.NewLine + " Please close the program or on HandHeld " + System.Environment.NewLine + "No database file", "Information");
                    //Executing.Instance.Insert_Log("PC_LoadData", ex.Message.ToString(), "Class_CopyFromDevice", "btnReceiveData_Click");
                    // Write logging
                    using (Execution _execute = new Execution())
                    {
                        _execute.Insert_Log("PCLoadData", ex.Message.ToString().Trim(), "frmExportForms.cs", "btnReceiveData_Click");
                    }
                }
            }
        }
        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            //Export To Excel.
            string ErrorTabCode = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                #region Comment Export

                /* Comment
                if (this.tabControl1.SelectedIndex == 0)
                {
                    #region Tab 1

                    //Check row count in datagridview.
                    if (this.dgvGIRefDelivery.Rows.Count > 0)
                    {
                        //string path = "C:\\ScanExport";
                        //DirectoryInfo directoryInfo = new DirectoryInfo(path);
                        //if (!Directory.Exists(path))
                        //{
                        //    Directory.CreateDirectory(path);
                        //}

                        //Calling class export to excel files use library excel.
                        //ExportData.OnExportDataToExcel("1.1.2 GI Ref. Delivery", dgvGIRefDelivery);

                        //Calling class export to excel files
                        ErrorTabCode = "EXPORTTAB01";
                        string _message = ExportData.OnExportDataToDatagridview(dgvGIRefDelivery);
                        if (_message != string.Empty)
                        {
                            //Show message is save complete.
                            ClassMsg.DialogInfomation(_message);
                        }
                    }

                    #endregion
                }
                else if (this.tabControl1.SelectedIndex == 1)
                {
                    #region Tab 2

                    //Check row count in datagridview.
                    if (this.dgvDistributionRet.Rows.Count > 0)
                    {
                        //string path = "C:\\ScanExport";
                        //DirectoryInfo directoryInfo = new DirectoryInfo(path);
                        //if (!Directory.Exists(path))
                        //{
                        //    Directory.CreateDirectory(path);
                        //}

                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTTAB02";
                        string _message = ExportData.OnExportDataToDatagridview(dgvDistributionRet);
                        if (_message != string.Empty)
                        {
                            //Show message is save complete.
                            ClassMsg.DialogInfomation(_message);
                        }
                    }

                    #endregion
                }
                else if (this.tabControl1.SelectedIndex == 2)
                {
                    #region Tab 3

                    if (this.dgvGRFromProduction.Rows.Count > 0)
                    {
                        //string path = "C:\\ScanExport";
                        //DirectoryInfo directoryInfo = new DirectoryInfo(path);
                        //if (!Directory.Exists(path))
                        //{
                        //    Directory.CreateDirectory(path);
                        //}

                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTTAB03";
                        string _message = ExportData.OnExportDataToDatagridview(dgvGRFromProduction);
                        if (_message != string.Empty)
                        {
                            //Show message is save complete.
                            ClassMsg.DialogInfomation(_message);
                        }
                    }

                    #endregion
                }
                else if (this.tabControl1.SelectedIndex == 3)
                {
                    #region Tab 4

                    if (this.dgvGIRefIOEmpty.Rows.Count > 0)
                    {
                        //string path = "C:\\ScanExport";
                        //DirectoryInfo directoryInfo = new DirectoryInfo(path);
                        //if (!Directory.Exists(path))
                        //{
                        //    Directory.CreateDirectory(path);
                        //}

                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTTAB04";
                        string _message = ExportData.OnExportDataToDatagridview(dgvGIRefIOEmpty);
                        if (_message != string.Empty)
                        {
                            //Show message is save complete.
                            ClassMsg.DialogInfomation(_message);
                        }
                    }

                    #endregion
                }
                */

                #endregion

                if (this.tabControl1.SelectedIndex == 0)
                {
                    #region Tab 5

                    if (this.dgvWarehouse.Rows.Count > 0)
                    {
                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTTAB05";
                        string _message = ExportData.OnExportDataToDatagridview(dgvWarehouse);
                        if (_message != string.Empty)
                        {
                            //Show message is save complete.
                            ClassMsg.DialogInfomation(_message);
                        }
                    }
                    else
                    {
                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Not found Data Warehouse for Export file");
                        return;
                    }

                    #endregion
                }
                else if (this.tabControl1.SelectedIndex == 1)
                {
                    #region Tab 6

                    if (this.dgvDelivery.Rows.Count > 0)
                    {
                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTTAB06";
                        string _message = ExportData.OnExportDataToDatagridview(dgvDelivery);
                        if (_message != string.Empty)
                        {
                            //Show message is save complete.
                            ClassMsg.DialogInfomation(_message);
                        }
                    }
                    else
                    {
                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Not found Data Delivery for Export file");
                        return;
                    }

                    #endregion
                }
                else if (this.tabControl1.SelectedIndex == 2)
                {
                    #region Tab 7

                    if (this.dgvNewBarcode.Rows.Count > 0)
                    {
                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTTAB05";
                        string _message = ExportData.OnExportDataToDatagridview(dgvNewBarcode);
                        if (_message != string.Empty)
                        {
                            //Show message is save complete.
                            ClassMsg.DialogInfomation(_message);
                        }
                    }
                    else
                    {
                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Not found information for New Barcode Export file.");
                        return;
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                // Write logging
                using (Execution _execute = new Execution())
                {
                    _execute.Insert_Log(string.IsNullOrEmpty(ErrorTabCode) ? "EXPORT_TAB_EMPTY" : ErrorTabCode.Trim(), ex.Message.ToString().Trim(), "frmExportForms.cs", "btnExportToExcel_Click");
                }

                // Message display
                ClassMsg.DialogError("Error export to excel files : " + ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void btnExportSN_Click(object sender, EventArgs e)
        {
            string ErrorTabCode = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.tabControl1.SelectedIndex == 0)
                {
                    #region Tab 5

                    if (this.dgvWarehouseCylinder.Rows.Count > 0 && this.dgvWarehouse.Rows.Count > 0)
                    {
                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTSN01";
                        string _fileType = string.IsNullOrEmpty(ConfigurationManager.AppSettings["CylinderFileType"].ToString()) ? "TEXT" : ConfigurationManager.AppSettings["CylinderFileType"].ToString().Trim();
                        if (!string.IsNullOrEmpty(_fileType))
                        {
                            if (_fileType.Trim() == "TEXT")
                            {
                                #region EXPORT TEXT FILE

                                // Create the CSV file to which grid data will be exported.
                                SaveFileDialog sv = new SaveFileDialog();
                                //sv.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                                sv.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                                if (sv.ShowDialog() == DialogResult.OK)
                                {
                                    StreamWriter sw = new StreamWriter(sv.FileName, false, Encoding.Unicode);
                                    // Now write all the rows.
                                    foreach (DataGridViewRow dr in dgvWarehouseCylinder.Rows)
                                    {
                                        for (int i = 0; i < dgvWarehouseCylinder.Columns.Count; i++)
                                        {
                                            if (dgvWarehouseCylinder.Columns[i].Visible == true)
                                            {
                                                if (dr.Cells[i].Value != null)
                                                {
                                                    if (dr.Cells[i].GetType().ToString() == "System.DateTime")
                                                    {
                                                        sw.Write(Convert.ToDateTime(dr.Cells[i].Value).ToString("dd/MM/yyyy HH:mm:ss"));
                                                    }
                                                    else
                                                    {
                                                        sw.Write(dr.Cells[i].Value.ToString().Replace("\r\n", ""));
                                                    }
                                                }
                                                if (i < dgvWarehouseCylinder.Columns.Count - 1)
                                                {
                                                    sw.Write("\t");
                                                }
                                            }
                                        }
                                        sw.Write(sw.NewLine);
                                    }
                                    sw.Close();

                                    ClassMsg.DialogInfomation("Data Export completed");
                                }

                                #endregion
                            }
                            else if (_fileType.Trim() == "EXCEL")
                            {
                                #region EXPORT EXCEL FILE

                                string _message = ExportData.OnExportDataToDatagridview(dgvWarehouseCylinder);
                                if (_message != string.Empty)
                                {
                                    //Show message is save complete.
                                    ClassMsg.DialogInfomation(_message);
                                }

                                #endregion
                            }
                        }
                        else
                        {
                            #region EXPORT TEXT FILE

                            // Create the CSV file to which grid data will be exported.
                            SaveFileDialog sv = new SaveFileDialog();
                            //sv.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                            sv.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                            if (sv.ShowDialog() == DialogResult.OK)
                            {
                                StreamWriter sw = new StreamWriter(sv.FileName, false, Encoding.Unicode);
                                // Now write all the rows.
                                foreach (DataGridViewRow dr in dgvWarehouseCylinder.Rows)
                                {
                                    for (int i = 0; i < dgvWarehouseCylinder.Columns.Count; i++)
                                    {
                                        if (dgvWarehouseCylinder.Columns[i].Visible == true)
                                        {
                                            if (dr.Cells[i].Value != null)
                                            {
                                                if (dr.Cells[i].GetType().ToString() == "System.DateTime")
                                                {
                                                    sw.Write(Convert.ToDateTime(dr.Cells[i].Value).ToString("dd/MM/yyyy HH:mm:ss"));
                                                }
                                                else
                                                {
                                                    sw.Write(dr.Cells[i].Value.ToString().Replace("\r\n", ""));
                                                }
                                            }
                                            if (i < dgvWarehouseCylinder.Columns.Count - 1)
                                            {
                                                sw.Write("\t");
                                            }
                                        }
                                    }
                                    sw.Write(sw.NewLine);
                                }
                                sw.Close();

                                ClassMsg.DialogInfomation("Data Export completed");
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Not found Data Warehouse for Export file S/N");
                        return;
                    }

                    #endregion
                }
                else if (this.tabControl1.SelectedIndex == 1)
                {
                    #region Tab 6

                    if (this.dgvDeliveryCylinder.Rows.Count > 0 && this.dgvDelivery.Rows.Count > 0)
                    {
                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTSN02";
                        string _fileType = string.IsNullOrEmpty(ConfigurationManager.AppSettings["CylinderFileType"].ToString()) ? "TEXT" : ConfigurationManager.AppSettings["CylinderFileType"].ToString().Trim();
                        if (!string.IsNullOrEmpty(_fileType))
                        {
                            if (_fileType.Trim() == "TEXT")
                            {
                                #region EXPORT TEXT FILE

                                // Create the CSV file to which grid data will be exported.
                                SaveFileDialog sv = new SaveFileDialog();
                                //sv.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                                sv.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                                if (sv.ShowDialog() == DialogResult.OK)
                                {
                                    StreamWriter sw = new StreamWriter(sv.FileName, false, Encoding.Unicode);
                                    // Now write all the rows.
                                    foreach (DataGridViewRow dr in dgvDeliveryCylinder.Rows)
                                    {
                                        for (int i = 0; i < dgvDeliveryCylinder.Columns.Count; i++)
                                        {
                                            if (dgvDeliveryCylinder.Columns[i].Visible == true)
                                            {
                                                if (dr.Cells[i].Value != null)
                                                {
                                                    if (dr.Cells[i].GetType().ToString() == "System.DateTime")
                                                    {
                                                        sw.Write(Convert.ToDateTime(dr.Cells[i].Value).ToString("dd/MM/yyyy HH:mm:ss"));
                                                    }
                                                    else
                                                    {
                                                        sw.Write(dr.Cells[i].Value.ToString().Replace("\r\n", ""));
                                                    }
                                                }
                                                if (i < dgvDeliveryCylinder.Columns.Count - 1)
                                                {
                                                    sw.Write("\t");
                                                }
                                            }
                                        }
                                        sw.Write(sw.NewLine);
                                    }
                                    sw.Close();

                                    ClassMsg.DialogInfomation("Data Export completed");
                                }

                                #endregion
                            }
                            else if (_fileType.Trim() == "EXCEL")
                            {
                                #region EXPORT EXCEL FILE

                                string _message = ExportData.OnExportDataToDatagridview(dgvDeliveryCylinder);
                                if (_message != string.Empty)
                                {
                                    //Show message is save complete.
                                    ClassMsg.DialogInfomation(_message);
                                }

                                #endregion
                            }
                        }
                        else
                        {
                            #region EXPORT TEXT FILE

                            // Create the CSV file to which grid data will be exported.
                            SaveFileDialog sv = new SaveFileDialog();
                            //sv.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                            sv.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                            if (sv.ShowDialog() == DialogResult.OK)
                            {
                                StreamWriter sw = new StreamWriter(sv.FileName, false, Encoding.Unicode);
                                // Now write all the rows.
                                foreach (DataGridViewRow dr in dgvDeliveryCylinder.Rows)
                                {
                                    for (int i = 0; i < dgvDeliveryCylinder.Columns.Count; i++)
                                    {
                                        if (dgvDeliveryCylinder.Columns[i].Visible == true)
                                        {
                                            if (dr.Cells[i].Value != null)
                                            {
                                                if (dr.Cells[i].GetType().ToString() == "System.DateTime")
                                                {
                                                    sw.Write(Convert.ToDateTime(dr.Cells[i].Value).ToString("dd/MM/yyyy HH:mm:ss"));
                                                }
                                                else
                                                {
                                                    sw.Write(dr.Cells[i].Value.ToString().Replace("\r\n", ""));
                                                }
                                            }
                                            if (i < dgvDeliveryCylinder.Columns.Count - 1)
                                            {
                                                sw.Write("\t");
                                            }
                                        }
                                    }
                                    sw.Write(sw.NewLine);
                                }
                                sw.Close();

                                ClassMsg.DialogInfomation("Data Export completed");
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Not found Data Delivery for Export file S/N");
                        return;
                    }

                    #endregion
                }
                else if (this.tabControl1.SelectedIndex == 2)
                {
                    #region Tab 7

                    if (this.dgvNewBarcodeCylinder.Rows.Count > 0 && this.dgvNewBarcode.Rows.Count > 0)
                    {
                        //Calling class export to excel files.
                        ErrorTabCode = "EXPORTSN02";
                        string _fileType = string.IsNullOrEmpty(ConfigurationManager.AppSettings["CylinderFileType"].ToString()) ? "TEXT" : ConfigurationManager.AppSettings["CylinderFileType"].ToString().Trim();
                        if (!string.IsNullOrEmpty(_fileType))
                        {
                            if (_fileType.Trim() == "TEXT")
                            {
                                #region EXPORT TEXT FILE

                                // Create the CSV file to which grid data will be exported.
                                SaveFileDialog sv = new SaveFileDialog();
                                //sv.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                                sv.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                                if (sv.ShowDialog() == DialogResult.OK)
                                {
                                    StreamWriter sw = new StreamWriter(sv.FileName, false, Encoding.Unicode);
                                    // Now write all the rows.
                                    foreach (DataGridViewRow dr in dgvNewBarcodeCylinder.Rows)
                                    {
                                        for (int i = 0; i < dgvDeliveryCylinder.Columns.Count; i++)
                                        {
                                            if (dgvNewBarcodeCylinder.Columns[i].Visible == true)
                                            {
                                                if (dr.Cells[i].Value != null)
                                                {
                                                    if (dr.Cells[i].GetType().ToString() == "System.DateTime")
                                                    {
                                                        sw.Write(Convert.ToDateTime(dr.Cells[i].Value).ToString("dd/MM/yyyy HH:mm:ss"));
                                                    }
                                                    else
                                                    {
                                                        sw.Write(dr.Cells[i].Value.ToString().Replace("\r\n", ""));
                                                    }
                                                }
                                                if (i < dgvNewBarcodeCylinder.Columns.Count - 1)
                                                {
                                                    sw.Write("\t");
                                                }
                                            }
                                        }
                                        sw.Write(sw.NewLine);
                                    }
                                    sw.Close();

                                    ClassMsg.DialogInfomation("Data Export completed");
                                }

                                #endregion
                            }
                            else if (_fileType.Trim() == "EXCEL")
                            {
                                #region EXPORT EXCEL FILE

                                string _message = ExportData.OnExportDataToDatagridview(dgvNewBarcodeCylinder);
                                if (_message != string.Empty)
                                {
                                    //Show message is save complete.
                                    ClassMsg.DialogInfomation(_message);
                                }

                                #endregion
                            }
                        }
                        else
                        {
                            #region EXPORT TEXT FILE

                            // Create the CSV file to which grid data will be exported.
                            SaveFileDialog sv = new SaveFileDialog();
                            //sv.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                            sv.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                            if (sv.ShowDialog() == DialogResult.OK)
                            {
                                StreamWriter sw = new StreamWriter(sv.FileName, false, Encoding.Unicode);
                                // Now write all the rows.
                                foreach (DataGridViewRow dr in dgvNewBarcodeCylinder.Rows)
                                {
                                    for (int i = 0; i < dgvDeliveryCylinder.Columns.Count; i++)
                                    {
                                        if (dgvNewBarcodeCylinder.Columns[i].Visible == true)
                                        {
                                            if (dr.Cells[i].Value != null)
                                            {
                                                if (dr.Cells[i].GetType().ToString() == "System.DateTime")
                                                {
                                                    sw.Write(Convert.ToDateTime(dr.Cells[i].Value).ToString("dd/MM/yyyy HH:mm:ss"));
                                                }
                                                else
                                                {
                                                    sw.Write(dr.Cells[i].Value.ToString().Replace("\r\n", ""));
                                                }
                                            }
                                            if (i < dgvNewBarcodeCylinder.Columns.Count - 1)
                                            {
                                                sw.Write("\t");
                                            }
                                        }
                                    }
                                    sw.Write(sw.NewLine);
                                }
                                sw.Close();

                                ClassMsg.DialogInfomation("Data Export completed");
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        MsgBox.ClassMsgBox.ClassMsg.DialogWarning("Not found Data New Barcode for Export file S/N");
                        return;
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                // Write logging
                using (Execution _execute = new Execution())
                {
                    _execute.Insert_Log(string.IsNullOrEmpty(ErrorTabCode) ? "EXPORT_TAB_EMPTY" : ErrorTabCode.Trim(), ex.Message.ToString().Trim(), "frmExportForms.cs", "btnExportSN_Click");
                }

                // Message display
                ClassMsg.DialogError("Error export s/n files : " + ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Now.ToString("dd/MM/yy").Trim();
            lblTimeRun.Text = DateTime.Now.ToString("HH:mm:ss").Trim();

            ConnectDevice();
        }

        private void btnActivateLicence_Click(object sender, EventArgs e)
        {
            /*
            using (Activates.frmActivateLicense fActivate = new Activates.frmActivateLicense("ACTIVATES"))
            {
                fActivate.ShowDialog();
            }
            */

            try
            {
                if (!string.IsNullOrEmpty(Executing.Instance.getDateExpireSoftware()))
                {
                    if (Executing.Instance.getDateExpireSoftware() != "FALSE" &&
                        Executing.Instance.getDateExpireSoftware() != "LIFETIME" &&
                        Executing.Instance.getDateExpireSoftware() != "EXPIRE")
                    {
                        using (frmMessageActivateLicense fMessage = new frmMessageActivateLicense("WARNING", Executing.Instance.getDateExpireSoftware()))
                        {
                            fMessage.ShowDialog();
                        }
                    }
                    else if (Executing.Instance.getDateExpireSoftware() == "EXPIRE")
                    {
                        using (frmMessageActivateLicense fMessage = new frmMessageActivateLicense("EXPIRE_COMPLATE", string.Empty))
                        {
                            fMessage.ShowDialog();
                            Application.Exit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Write logging
                using (Execution _execute = new Execution())
                {
                    _execute.Insert_Log("ActivateClick", ex.Message.ToString().Trim(), "frmExportForms.cs", "btnActivateLicence_Click");
                }
            }
        }
        private void btnViewLogging_Click(object sender, EventArgs e)
        {
            using (frmLogging fLogging = new frmLogging())
            {
                fLogging.ShowDialog();
            }
        }

        #endregion
    }
}