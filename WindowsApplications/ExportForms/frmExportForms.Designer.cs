﻿namespace WindowsApplications.ExportForms
{
    partial class frmExportForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExportForms));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.btnReceiveData = new System.Windows.Forms.Button();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTimeRun = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.btnActivateLicence = new System.Windows.Forms.Button();
            this.lblNotify = new System.Windows.Forms.Label();
            this.btnExportSN = new System.Windows.Forms.Button();
            this.btnExportToExcel = new System.Windows.Forms.Button();
            this.lblResultSync = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlData = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvGIRefDelivery = new System.Windows.Forms.DataGridView();
            this.cCreate_Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDo_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cSerial_Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvDistributionRet = new System.Windows.Forms.DataGridView();
            this.cret_create_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cempty_or_full = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ccust_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doc_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serial_number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgvGRFromProduction = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cpre_order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgvGIRefIOEmpty = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dgvWarehouseCylinder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvWarehouse = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dgvDeliveryCylinder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvDelivery = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.dgvNewBarcodeCylinder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvNewBarcode = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.btnViewLogging = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHeader.SuspendLayout();
            this.pnlFooter.SuspendLayout();
            this.pnlData.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGIRefDelivery)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDistributionRet)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGRFromProduction)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGIRefIOEmpty)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWarehouseCylinder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWarehouse)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryCylinder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDelivery)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewBarcodeCylinder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewBarcode)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.Color.White;
            this.pnlHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlHeader.Controls.Add(this.label5);
            this.pnlHeader.Controls.Add(this.label2);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1069, 70);
            this.pnlHeader.TabIndex = 1;
            this.pnlHeader.Tag = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(15, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(425, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "copyright © 2016. Thai-Japan Gas Co.,Ltd. all rights reserved";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(11, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(400, 33);
            this.label2.TabIndex = 2;
            this.label2.Text = "[TOMOE] Barcode Software";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblVersion.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblVersion.Location = new System.Drawing.Point(310, 15);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(113, 16);
            this.lblVersion.TabIndex = 1;
            this.lblVersion.Text = "Version : x.x.x.x";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnReceiveData
            // 
            this.btnReceiveData.BackColor = System.Drawing.Color.DarkBlue;
            this.btnReceiveData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnReceiveData.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnReceiveData.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnReceiveData.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnReceiveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReceiveData.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnReceiveData.ForeColor = System.Drawing.Color.White;
            this.btnReceiveData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReceiveData.Location = new System.Drawing.Point(18, 86);
            this.btnReceiveData.Name = "btnReceiveData";
            this.btnReceiveData.Size = new System.Drawing.Size(182, 42);
            this.btnReceiveData.TabIndex = 2;
            this.btnReceiveData.Tag = "";
            this.btnReceiveData.Text = "Download Data From PDA";
            this.btnReceiveData.UseVisualStyleBackColor = false;
            this.btnReceiveData.Click += new System.EventHandler(this.btnReceiveData_Click);
            // 
            // pnlFooter
            // 
            this.pnlFooter.BackColor = System.Drawing.Color.White;
            this.pnlFooter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFooter.Controls.Add(this.label4);
            this.pnlFooter.Controls.Add(this.label3);
            this.pnlFooter.Controls.Add(this.lblTimeRun);
            this.pnlFooter.Controls.Add(this.lblDate);
            this.pnlFooter.Controls.Add(this.btnActivateLicence);
            this.pnlFooter.Controls.Add(this.lblVersion);
            this.pnlFooter.Controls.Add(this.lblNotify);
            this.pnlFooter.Controls.Add(this.btnExportSN);
            this.pnlFooter.Controls.Add(this.btnExportToExcel);
            this.pnlFooter.Controls.Add(this.lblResultSync);
            this.pnlFooter.Controls.Add(this.label1);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.Location = new System.Drawing.Point(0, 503);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Size = new System.Drawing.Size(1069, 47);
            this.pnlFooter.TabIndex = 3;
            this.pnlFooter.Tag = "1";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(559, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 16);
            this.label4.TabIndex = 26;
            this.label4.Text = "Time :";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(445, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 25;
            this.label3.Text = "Date :";
            // 
            // lblTimeRun
            // 
            this.lblTimeRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTimeRun.ForeColor = System.Drawing.Color.Magenta;
            this.lblTimeRun.Location = new System.Drawing.Point(605, 15);
            this.lblTimeRun.Name = "lblTimeRun";
            this.lblTimeRun.Size = new System.Drawing.Size(82, 18);
            this.lblTimeRun.TabIndex = 23;
            this.lblTimeRun.Text = "HH:mm:ss";
            // 
            // lblDate
            // 
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblDate.ForeColor = System.Drawing.Color.Magenta;
            this.lblDate.Location = new System.Drawing.Point(491, 15);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(77, 18);
            this.lblDate.TabIndex = 24;
            this.lblDate.Text = "dd/MM/yy";
            // 
            // btnActivateLicence
            // 
            this.btnActivateLicence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActivateLicence.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnActivateLicence.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnActivateLicence.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnActivateLicence.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnActivateLicence.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnActivateLicence.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActivateLicence.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnActivateLicence.ForeColor = System.Drawing.Color.White;
            this.btnActivateLicence.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActivateLicence.Location = new System.Drawing.Point(698, 8);
            this.btnActivateLicence.Name = "btnActivateLicence";
            this.btnActivateLicence.Size = new System.Drawing.Size(167, 29);
            this.btnActivateLicence.TabIndex = 20;
            this.btnActivateLicence.Tag = "";
            this.btnActivateLicence.Text = "Activate Licences";
            this.btnActivateLicence.UseVisualStyleBackColor = false;
            this.btnActivateLicence.Click += new System.EventHandler(this.btnActivateLicence_Click);
            // 
            // lblNotify
            // 
            this.lblNotify.AutoSize = true;
            this.lblNotify.BackColor = System.Drawing.Color.Transparent;
            this.lblNotify.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblNotify.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblNotify.Location = new System.Drawing.Point(692, 10);
            this.lblNotify.Name = "lblNotify";
            this.lblNotify.Size = new System.Drawing.Size(0, 24);
            this.lblNotify.TabIndex = 18;
            // 
            // btnExportSN
            // 
            this.btnExportSN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportSN.BackColor = System.Drawing.Color.LimeGreen;
            this.btnExportSN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExportSN.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExportSN.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnExportSN.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnExportSN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportSN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnExportSN.ForeColor = System.Drawing.Color.White;
            this.btnExportSN.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportSN.Location = new System.Drawing.Point(968, 8);
            this.btnExportSN.Name = "btnExportSN";
            this.btnExportSN.Size = new System.Drawing.Size(91, 29);
            this.btnExportSN.TabIndex = 3;
            this.btnExportSN.Tag = "";
            this.btnExportSN.Text = "Export S/N";
            this.btnExportSN.UseVisualStyleBackColor = false;
            this.btnExportSN.Click += new System.EventHandler(this.btnExportSN_Click);
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportToExcel.BackColor = System.Drawing.Color.DarkBlue;
            this.btnExportToExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExportToExcel.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExportToExcel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnExportToExcel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnExportToExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportToExcel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnExportToExcel.ForeColor = System.Drawing.Color.White;
            this.btnExportToExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportToExcel.Location = new System.Drawing.Point(871, 8);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(91, 29);
            this.btnExportToExcel.TabIndex = 3;
            this.btnExportToExcel.Tag = "";
            this.btnExportToExcel.Text = "Export Full";
            this.btnExportToExcel.UseVisualStyleBackColor = false;
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // lblResultSync
            // 
            this.lblResultSync.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResultSync.AutoSize = true;
            this.lblResultSync.BackColor = System.Drawing.Color.Transparent;
            this.lblResultSync.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblResultSync.ForeColor = System.Drawing.Color.Black;
            this.lblResultSync.Location = new System.Drawing.Point(168, 15);
            this.lblResultSync.Name = "lblResultSync";
            this.lblResultSync.Size = new System.Drawing.Size(104, 16);
            this.lblResultSync.TabIndex = 0;
            this.lblResultSync.Text = "Sync HandHeld";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "(PDA) Sync Computer :";
            // 
            // pnlData
            // 
            this.pnlData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlData.BackColor = System.Drawing.Color.White;
            this.pnlData.Controls.Add(this.tabControl1);
            this.pnlData.Location = new System.Drawing.Point(0, 153);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(1069, 350);
            this.pnlData.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1069, 350);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvGIRefDelivery);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1061, 324);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "1.1.2 GI Ref. Delivery";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvGIRefDelivery
            // 
            this.dgvGIRefDelivery.AllowUserToAddRows = false;
            this.dgvGIRefDelivery.AllowUserToDeleteRows = false;
            this.dgvGIRefDelivery.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGIRefDelivery.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cCreate_Date,
            this.cDo_No,
            this.cSerial_Number});
            this.dgvGIRefDelivery.Location = new System.Drawing.Point(3, 3);
            this.dgvGIRefDelivery.Name = "dgvGIRefDelivery";
            this.dgvGIRefDelivery.ReadOnly = true;
            this.dgvGIRefDelivery.Size = new System.Drawing.Size(1055, 318);
            this.dgvGIRefDelivery.TabIndex = 0;
            // 
            // cCreate_Date
            // 
            this.cCreate_Date.DataPropertyName = "create_date";
            dataGridViewCellStyle1.Format = "dd/MM/yyHH:mm:ss";
            this.cCreate_Date.DefaultCellStyle = dataGridViewCellStyle1;
            this.cCreate_Date.HeaderText = "Create Date";
            this.cCreate_Date.Name = "cCreate_Date";
            this.cCreate_Date.ReadOnly = true;
            this.cCreate_Date.Width = 120;
            // 
            // cDo_No
            // 
            this.cDo_No.DataPropertyName = "do_no";
            this.cDo_No.HeaderText = "Invoice / DO";
            this.cDo_No.Name = "cDo_No";
            this.cDo_No.ReadOnly = true;
            this.cDo_No.Width = 120;
            // 
            // cSerial_Number
            // 
            this.cSerial_Number.DataPropertyName = "serial_number";
            this.cSerial_Number.HeaderText = "S/N";
            this.cSerial_Number.Name = "cSerial_Number";
            this.cSerial_Number.ReadOnly = true;
            this.cSerial_Number.Width = 120;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvDistributionRet);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1061, 324);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "1.2 Distribution Ret";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvDistributionRet
            // 
            this.dgvDistributionRet.AllowUserToAddRows = false;
            this.dgvDistributionRet.AllowUserToDeleteRows = false;
            this.dgvDistributionRet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDistributionRet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cret_create_date,
            this.cempty_or_full,
            this.ccust_id,
            this.doc_no,
            this.serial_number,
            this.vehicle});
            this.dgvDistributionRet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDistributionRet.Location = new System.Drawing.Point(3, 3);
            this.dgvDistributionRet.Name = "dgvDistributionRet";
            this.dgvDistributionRet.ReadOnly = true;
            this.dgvDistributionRet.Size = new System.Drawing.Size(1055, 318);
            this.dgvDistributionRet.TabIndex = 0;
            // 
            // cret_create_date
            // 
            this.cret_create_date.DataPropertyName = "create_date";
            dataGridViewCellStyle2.Format = "dd/MM/yyHH:mm:ss";
            this.cret_create_date.DefaultCellStyle = dataGridViewCellStyle2;
            this.cret_create_date.HeaderText = "Create Date";
            this.cret_create_date.Name = "cret_create_date";
            this.cret_create_date.ReadOnly = true;
            this.cret_create_date.Width = 120;
            // 
            // cempty_or_full
            // 
            this.cempty_or_full.DataPropertyName = "empty_or_full";
            this.cempty_or_full.HeaderText = "Empty / Full";
            this.cempty_or_full.Name = "cempty_or_full";
            this.cempty_or_full.ReadOnly = true;
            this.cempty_or_full.Width = 90;
            // 
            // ccust_id
            // 
            this.ccust_id.DataPropertyName = "cust_id";
            this.ccust_id.HeaderText = "Cust ID";
            this.ccust_id.Name = "ccust_id";
            this.ccust_id.ReadOnly = true;
            // 
            // doc_no
            // 
            this.doc_no.DataPropertyName = "doc_no";
            this.doc_no.HeaderText = "Document Number";
            this.doc_no.Name = "doc_no";
            this.doc_no.ReadOnly = true;
            this.doc_no.Width = 150;
            // 
            // serial_number
            // 
            this.serial_number.DataPropertyName = "serial_number";
            this.serial_number.HeaderText = "S/N";
            this.serial_number.Name = "serial_number";
            this.serial_number.ReadOnly = true;
            this.serial_number.Width = 120;
            // 
            // vehicle
            // 
            this.vehicle.DataPropertyName = "vehicle";
            this.vehicle.HeaderText = "Vehicle";
            this.vehicle.Name = "vehicle";
            this.vehicle.ReadOnly = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgvGRFromProduction);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1061, 324);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "1.4.1 GR From Production";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgvGRFromProduction
            // 
            this.dgvGRFromProduction.AllowUserToAddRows = false;
            this.dgvGRFromProduction.AllowUserToDeleteRows = false;
            this.dgvGRFromProduction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGRFromProduction.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.cpre_order,
            this.cbatch,
            this.dataGridViewTextBoxColumn31});
            this.dgvGRFromProduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGRFromProduction.Location = new System.Drawing.Point(3, 3);
            this.dgvGRFromProduction.Name = "dgvGRFromProduction";
            this.dgvGRFromProduction.ReadOnly = true;
            this.dgvGRFromProduction.Size = new System.Drawing.Size(1055, 318);
            this.dgvGRFromProduction.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "create_date";
            dataGridViewCellStyle3.Format = "dd/MM/yyHH:mm:ss";
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn26.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Width = 120;
            // 
            // cpre_order
            // 
            this.cpre_order.DataPropertyName = "pre_order";
            this.cpre_order.HeaderText = "Pre Order";
            this.cpre_order.Name = "cpre_order";
            this.cpre_order.ReadOnly = true;
            // 
            // cbatch
            // 
            this.cbatch.DataPropertyName = "batch";
            this.cbatch.HeaderText = "Batch";
            this.cbatch.Name = "cbatch";
            this.cbatch.ReadOnly = true;
            this.cbatch.Width = 110;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "serial_number";
            this.dataGridViewTextBoxColumn31.HeaderText = "S/N";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 120;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgvGIRefIOEmpty);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1061, 324);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "1.5.3 GI Ref. IO Empty";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgvGIRefIOEmpty
            // 
            this.dgvGIRefIOEmpty.AllowUserToAddRows = false;
            this.dgvGIRefIOEmpty.AllowUserToDeleteRows = false;
            this.dgvGIRefIOEmpty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGIRefIOEmpty.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29});
            this.dgvGIRefIOEmpty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGIRefIOEmpty.Location = new System.Drawing.Point(3, 3);
            this.dgvGIRefIOEmpty.Name = "dgvGIRefIOEmpty";
            this.dgvGIRefIOEmpty.ReadOnly = true;
            this.dgvGIRefIOEmpty.Size = new System.Drawing.Size(1055, 318);
            this.dgvGIRefIOEmpty.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "create_date";
            dataGridViewCellStyle4.Format = "dd/MM/yyHH:mm:ss";
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn27.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Width = 120;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "do_no";
            this.dataGridViewTextBoxColumn28.HeaderText = "Invoice / DO";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 120;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "serial_number";
            this.dataGridViewTextBoxColumn29.HeaderText = "S/N";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Width = 120;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dgvWarehouseCylinder);
            this.tabPage5.Controls.Add(this.dgvWarehouse);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1061, 324);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "1. Warehouse";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dgvWarehouseCylinder
            // 
            this.dgvWarehouseCylinder.AllowUserToAddRows = false;
            this.dgvWarehouseCylinder.AllowUserToDeleteRows = false;
            this.dgvWarehouseCylinder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvWarehouseCylinder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWarehouseCylinder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn48});
            this.dgvWarehouseCylinder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWarehouseCylinder.Location = new System.Drawing.Point(3, 3);
            this.dgvWarehouseCylinder.Name = "dgvWarehouseCylinder";
            this.dgvWarehouseCylinder.ReadOnly = true;
            this.dgvWarehouseCylinder.Size = new System.Drawing.Size(1055, 318);
            this.dgvWarehouseCylinder.TabIndex = 2;
            this.dgvWarehouseCylinder.Visible = false;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "cylinder_sn";
            this.dataGridViewTextBoxColumn48.HeaderText = "Cylinder Number";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            this.dataGridViewTextBoxColumn48.Width = 110;
            // 
            // dgvWarehouse
            // 
            this.dgvWarehouse.AllowUserToAddRows = false;
            this.dgvWarehouse.AllowUserToDeleteRows = false;
            this.dgvWarehouse.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvWarehouse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWarehouse.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn30,
            this.Column1,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgvWarehouse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWarehouse.Location = new System.Drawing.Point(3, 3);
            this.dgvWarehouse.Name = "dgvWarehouse";
            this.dgvWarehouse.ReadOnly = true;
            this.dgvWarehouse.Size = new System.Drawing.Size(1055, 318);
            this.dgvWarehouse.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "cDate";
            dataGridViewCellStyle5.Format = "dd/MM/yy";
            this.dataGridViewTextBoxColumn30.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn30.HeaderText = "Date";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "cTime";
            dataGridViewCellStyle6.Format = "HH:mm:ss";
            this.Column1.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column1.HeaderText = "Time";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 80;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "item_code";
            this.dataGridViewTextBoxColumn32.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 110;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "sell_order_no";
            this.dataGridViewTextBoxColumn33.HeaderText = "Sale Order Number";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Width = 120;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "cylinder_sn";
            this.Column2.HeaderText = "Cylinder Number";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 110;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "customer_code";
            this.Column3.HeaderText = "Customer Code";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 110;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "customer_name";
            this.Column4.HeaderText = "Customer Name";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 200;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.dgvDeliveryCylinder);
            this.tabPage6.Controls.Add(this.dgvDelivery);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1061, 324);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "2. Delivery";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // dgvDeliveryCylinder
            // 
            this.dgvDeliveryCylinder.AllowUserToAddRows = false;
            this.dgvDeliveryCylinder.AllowUserToDeleteRows = false;
            this.dgvDeliveryCylinder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeliveryCylinder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn49});
            this.dgvDeliveryCylinder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDeliveryCylinder.Location = new System.Drawing.Point(3, 3);
            this.dgvDeliveryCylinder.Name = "dgvDeliveryCylinder";
            this.dgvDeliveryCylinder.ReadOnly = true;
            this.dgvDeliveryCylinder.Size = new System.Drawing.Size(1055, 318);
            this.dgvDeliveryCylinder.TabIndex = 3;
            this.dgvDeliveryCylinder.Visible = false;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "cylinder_sn";
            this.dataGridViewTextBoxColumn49.HeaderText = "Cylinder Number";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            this.dataGridViewTextBoxColumn49.Width = 110;
            // 
            // dgvDelivery
            // 
            this.dgvDelivery.AllowUserToAddRows = false;
            this.dgvDelivery.AllowUserToDeleteRows = false;
            this.dgvDelivery.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDelivery.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.Column5,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.Column6,
            this.Column7});
            this.dgvDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDelivery.Location = new System.Drawing.Point(3, 3);
            this.dgvDelivery.Name = "dgvDelivery";
            this.dgvDelivery.ReadOnly = true;
            this.dgvDelivery.Size = new System.Drawing.Size(1055, 318);
            this.dgvDelivery.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "cDate";
            dataGridViewCellStyle7.Format = "dd/MM/yy";
            this.dataGridViewTextBoxColumn34.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn34.HeaderText = "Date";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "cTime";
            dataGridViewCellStyle8.Format = "HH:mm:ss";
            this.dataGridViewTextBoxColumn35.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn35.HeaderText = "Time";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.Width = 80;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "item_code";
            this.dataGridViewTextBoxColumn36.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Width = 110;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "factory_code";
            this.Column5.HeaderText = "TMSB Factory Code";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 140;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "cylinder_sn";
            this.dataGridViewTextBoxColumn38.HeaderText = "Cylinder Number";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 110;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "cust_id";
            this.dataGridViewTextBoxColumn39.HeaderText = "Customer Code";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 110;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "cust_name";
            this.dataGridViewTextBoxColumn40.HeaderText = "Customer Name";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Width = 200;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "vehicle";
            this.Column6.HeaderText = "Vehicle";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "empty_or_full";
            this.Column7.HeaderText = "Full/Empty";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.dgvNewBarcodeCylinder);
            this.tabPage7.Controls.Add(this.dgvNewBarcode);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1061, 324);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "3. New Barcode";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // dgvNewBarcodeCylinder
            // 
            this.dgvNewBarcodeCylinder.AllowUserToAddRows = false;
            this.dgvNewBarcodeCylinder.AllowUserToDeleteRows = false;
            this.dgvNewBarcodeCylinder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewBarcodeCylinder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn50});
            this.dgvNewBarcodeCylinder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvNewBarcodeCylinder.Location = new System.Drawing.Point(3, 3);
            this.dgvNewBarcodeCylinder.Name = "dgvNewBarcodeCylinder";
            this.dgvNewBarcodeCylinder.ReadOnly = true;
            this.dgvNewBarcodeCylinder.Size = new System.Drawing.Size(1055, 318);
            this.dgvNewBarcodeCylinder.TabIndex = 3;
            this.dgvNewBarcodeCylinder.Visible = false;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.DataPropertyName = "scan_number";
            this.dataGridViewTextBoxColumn50.HeaderText = "Cylinder Number";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            this.dataGridViewTextBoxColumn50.Width = 110;
            // 
            // dgvNewBarcode
            // 
            this.dgvNewBarcode.AllowUserToAddRows = false;
            this.dgvNewBarcode.AllowUserToDeleteRows = false;
            this.dgvNewBarcode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewBarcode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.Column8,
            this.dataGridViewTextBoxColumn44});
            this.dgvNewBarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvNewBarcode.Location = new System.Drawing.Point(3, 3);
            this.dgvNewBarcode.Name = "dgvNewBarcode";
            this.dgvNewBarcode.ReadOnly = true;
            this.dgvNewBarcode.Size = new System.Drawing.Size(1055, 318);
            this.dgvNewBarcode.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "cDate";
            dataGridViewCellStyle9.Format = "dd/MM/yy";
            this.dataGridViewTextBoxColumn37.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn37.HeaderText = "Date";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "cTime";
            dataGridViewCellStyle10.Format = "HH:mm:ss";
            this.dataGridViewTextBoxColumn41.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn41.HeaderText = "Time";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Width = 80;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "item_code";
            this.dataGridViewTextBoxColumn42.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 110;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "notes";
            this.Column8.HeaderText = "Lot Number";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 200;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "scan_number";
            this.dataGridViewTextBoxColumn44.HeaderText = "Cylinder Number";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Width = 110;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // btnViewLogging
            // 
            this.btnViewLogging.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnViewLogging.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnViewLogging.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnViewLogging.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Orange;
            this.btnViewLogging.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Khaki;
            this.btnViewLogging.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewLogging.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnViewLogging.ForeColor = System.Drawing.Color.White;
            this.btnViewLogging.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewLogging.Location = new System.Drawing.Point(210, 86);
            this.btnViewLogging.Name = "btnViewLogging";
            this.btnViewLogging.Size = new System.Drawing.Size(167, 42);
            this.btnViewLogging.TabIndex = 2;
            this.btnViewLogging.Tag = "";
            this.btnViewLogging.Text = "View Logging";
            this.btnViewLogging.UseVisualStyleBackColor = false;
            this.btnViewLogging.Click += new System.EventHandler(this.btnViewLogging_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "receive_date";
            dataGridViewCellStyle11.Format = "dd/MM/yyHH:mm:ss";
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn1.HeaderText = "Receive Date";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 110;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "vignette_no";
            this.dataGridViewTextBoxColumn2.HeaderText = "Vignette No";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 110;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "serial_number";
            this.dataGridViewTextBoxColumn3.HeaderText = "Serial Number";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 140;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "dot";
            dataGridViewCellStyle12.Format = "dd/MM/yyHH:mm:ss";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn4.HeaderText = "Dot";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 120;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "tire_size";
            this.dataGridViewTextBoxColumn5.HeaderText = "Tire Size";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "pettern";
            this.dataGridViewTextBoxColumn6.HeaderText = "Pettern";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "weight";
            this.dataGridViewTextBoxColumn7.HeaderText = "Weight";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 150;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "location";
            this.dataGridViewTextBoxColumn8.HeaderText = "Location";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 110;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "on_hand";
            this.dataGridViewTextBoxColumn9.HeaderText = "On Hand";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "owner";
            dataGridViewCellStyle13.Format = "dd/MM/yyHH:mm:ss";
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn10.HeaderText = "Owner";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 120;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "confidential";
            this.dataGridViewTextBoxColumn11.HeaderText = "Confidential";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 110;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "ctj_inv";
            this.dataGridViewTextBoxColumn12.HeaderText = "CTJ_INV";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 110;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "t_number";
            this.dataGridViewTextBoxColumn13.HeaderText = "T-Number";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 120;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "test_request";
            dataGridViewCellStyle14.Format = "dd/MM/yyHH:mm:ss";
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn14.HeaderText = "Test Request";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 110;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "li_and_si";
            this.dataGridViewTextBoxColumn15.HeaderText = "Li and Si";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 120;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "froms";
            this.dataGridViewTextBoxColumn16.HeaderText = "From";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 110;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "old_location";
            this.dataGridViewTextBoxColumn17.HeaderText = "Old Location";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 110;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "return_type";
            this.dataGridViewTextBoxColumn18.HeaderText = "Return Type";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "tran_type";
            this.dataGridViewTextBoxColumn19.HeaderText = "Transaction Type";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 150;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn20.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 110;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "serial_number";
            this.dataGridViewTextBoxColumn21.HeaderText = "Serial Number";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 110;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "location";
            this.dataGridViewTextBoxColumn22.HeaderText = "Location";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "new_location";
            this.dataGridViewTextBoxColumn23.HeaderText = "New Location";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "tran_type";
            this.dataGridViewTextBoxColumn24.HeaderText = "Transaction Type";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 120;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "create_date";
            this.dataGridViewTextBoxColumn25.HeaderText = "Create Date";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 120;
            // 
            // frmExportForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1069, 550);
            this.Controls.Add(this.pnlData);
            this.Controls.Add(this.pnlFooter);
            this.Controls.Add(this.btnViewLogging);
            this.Controls.Add(this.btnReceiveData);
            this.Controls.Add(this.pnlHeader);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmExportForms";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "[TOMOE] Barcode Software";
            this.Load += new System.EventHandler(this.frmExportForms_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmExportForms_KeyDown);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlFooter.ResumeLayout(false);
            this.pnlFooter.PerformLayout();
            this.pnlData.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGIRefDelivery)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDistributionRet)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGRFromProduction)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGIRefIOEmpty)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWarehouseCylinder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWarehouse)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryCylinder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDelivery)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewBarcodeCylinder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewBarcode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel pnlHeader;
        protected System.Windows.Forms.Button btnReceiveData;
        private System.Windows.Forms.Panel pnlFooter;
        protected System.Windows.Forms.Label lblResultSync;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.Button btnExportToExcel;
        internal System.Windows.Forms.Label lblNotify;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        protected System.Windows.Forms.Button btnActivateLicence;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgvGIRefDelivery;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCreate_Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDo_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn cSerial_Number;
        private System.Windows.Forms.DataGridView dgvDistributionRet;
        private System.Windows.Forms.DataGridViewTextBoxColumn cret_create_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn cempty_or_full;
        private System.Windows.Forms.DataGridViewTextBoxColumn ccust_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn doc_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn serial_number;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicle;
        private System.Windows.Forms.DataGridView dgvGRFromProduction;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn cpre_order;
        private System.Windows.Forms.DataGridViewTextBoxColumn cbatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridView dgvGIRefIOEmpty;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.Label lblVersion;
        protected System.Windows.Forms.Button btnViewLogging;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView dgvWarehouse;
        private System.Windows.Forms.DataGridView dgvDelivery;
        private System.Windows.Forms.DataGridView dgvNewBarcode;
        protected System.Windows.Forms.Button btnExportSN;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridView dgvWarehouseCylinder;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridView dgvDeliveryCylinder;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridView dgvNewBarcodeCylinder;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.Label lblTimeRun;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;

    }
}