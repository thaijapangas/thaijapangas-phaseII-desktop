﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Data;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using WindowsApplications.CallingService;
using WindowsApplications.Execute;

namespace WindowsApplications.Connections
{
    public partial class Connection
    {
        #region Property

        protected CallService dbManager;

        #endregion

        #region Method

        public Connection()
        {
            string connectionString = @"Data Source=C:\Data Base SDF\TJG.sdf;Password=pass;Persist Security Info=True";

            dbManager = new CallService(connectionString);
            try
            {
                dbManager.Open();
            }
            catch (SqlCeException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}