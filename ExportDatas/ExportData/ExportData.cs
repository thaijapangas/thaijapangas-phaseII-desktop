﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Data;
using System.Drawing;

using System.Threading;
using Excel = Microsoft.Office.Interop.Excel;
using MsgBox;
using MsgBox.ClassMsgBox;

namespace ExportDatas.ExportData
{
    public class ExportData
    {
        #region Export master data to excel.
        public static void ExportDataToExcel(string strName, DataTable dt)
        {
            Excel.Application xlApp;
            Excel._Workbook xlBook;
            Excel._Worksheet xlSheet;
            Excel.Range excelRange;
            object missing = System.Reflection.Missing.Value;

            if (ClassMsg.DialogConfirm("Do you want to export " + strName + " to excel ?") == System.Windows.Forms.DialogResult.Yes)
            {

                Cursor.Current = Cursors.WaitCursor;

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.FileName = strName + ".xls";
                sfd.Filter = "Excel File | *.xls;";
                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    xlApp = new Excel.Application();
                    xlApp.Visible = false;

                    xlApp.UserControl = false;

                    xlSheet = null;
                    excelRange = null;

                    xlBook = xlApp.Workbooks.Add(missing);
                    Excel.Sheets sheets = xlBook.Worksheets;

                    sheets.Add(System.Type.Missing, xlBook.Sheets[3], 1, Excel.XlSheetType.xlWorksheet);

                    //xlSheet.
                    xlSheet = (Excel._Worksheet)xlBook.Sheets[1];
                    xlSheet.Activate();
                    xlSheet.Name = strName;

                    // Header
                    excelRange = xlSheet.get_Range("B2", "B2");
                    excelRange.Font.Size = 14;
                    excelRange.Font.Name = "Arial";
                    excelRange.Font.Color = Color.Black.ToArgb();
                    excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                    excelRange.Value2 = strName;


                    // Column Name
                    int ascii = 66;
                    string colColumn = string.Empty;
                    string colData = string.Empty;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {

                        colColumn = char.ConvertFromUtf32(ascii) + "4";

                        excelRange = xlSheet.get_Range(colColumn, colColumn);
                        excelRange.Font.Size = 12;
                        excelRange.Font.Name = "Arial";
                        excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                        excelRange.Interior.Color = Color.FromArgb(192, 192, 192);
                        excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                        excelRange.Value2 = dt.Columns[i].Caption.ToString();

                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                        for (int r = 0; r < dt.Rows.Count; r++)
                        {
                            colData = char.ConvertFromUtf32(ascii) + (r + 5);
                            excelRange = xlSheet.get_Range(colData, colData);
                            excelRange.Font.Size = 10;
                            excelRange.Font.Name = "Arial";
                            excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                            excelRange.HorizontalAlignment = Excel.Constants.xlLeft;
                            excelRange.Value2 = dt.Rows[r][i].ToString();
                            excelRange.EntireColumn.AutoFit();
                        }

                        excelRange.EntireColumn.AutoFit();

                        ascii++;

                    }

                    excelRange = xlSheet.get_Range("B4", colData);
                    excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                    excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                    excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                    excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                    ClassMsg.DialogInfomation("Export " + strName + " to excel successed.");

                    Cursor.Current = Cursors.Default;

                    xlBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);

                    xlApp.Visible = true;

                }
            }
        }
        #endregion

        #region Export master data to excel for transaction.
        public static bool ExportToExcelFiles(string strName, DataTable dt)
        {
            return WorkData.Work.Working(delegate()
            {
                Excel.Application xlApp;
                Excel._Workbook xlBook;
                Excel._Worksheet xlSheet;
                Excel.Range excelRange;
                object missing = System.Reflection.Missing.Value;

                if (ClassMsg.DialogConfirm("Do you want to export " + strName + " to excel ?") == System.Windows.Forms.DialogResult.Yes)
                {

                    Cursor.Current = Cursors.WaitCursor;

                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.FileName = strName + ".xls";
                    sfd.Filter = "Excel File | *.xls;";
                    if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {

                        xlApp = new Excel.Application();
                        xlApp.Visible = false;

                        xlApp.UserControl = false;

                        xlSheet = null;
                        excelRange = null;

                        xlBook = xlApp.Workbooks.Add(missing);
                        Excel.Sheets sheets = xlBook.Worksheets;

                        sheets.Add(System.Type.Missing, xlBook.Sheets[3], 1, Excel.XlSheetType.xlWorksheet);

                        //xlSheet.
                        xlSheet = (Excel._Worksheet)xlBook.Sheets[1];
                        xlSheet.Activate();
                        xlSheet.Name = strName;

                        // Header
                        excelRange = xlSheet.get_Range("B2", "B2");
                        excelRange.Font.Size = 14;
                        excelRange.Font.Name = "Arial";
                        excelRange.Font.Color = Color.Black.ToArgb();
                        excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                        excelRange.Value2 = strName;


                        // Column Name
                        int ascii = 66;
                        string colColumn = string.Empty;
                        string colData = string.Empty;
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {

                            colColumn = char.ConvertFromUtf32(ascii) + "4";

                            excelRange = xlSheet.get_Range(colColumn, colColumn);
                            excelRange.Font.Size = 12;
                            excelRange.Font.Name = "Arial";
                            excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                            excelRange.Interior.Color = Color.FromArgb(192, 192, 192);
                            excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                            excelRange.Value2 = dt.Columns[i].Caption.ToString();

                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                            for (int r = 0; r < dt.Rows.Count; r++)
                            {
                                colData = char.ConvertFromUtf32(ascii) + (r + 5);
                                excelRange = xlSheet.get_Range(colData, colData);
                                excelRange.Font.Size = 10;
                                excelRange.Font.Name = "Arial";
                                excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                excelRange.HorizontalAlignment = Excel.Constants.xlLeft;
                                excelRange.Value2 = dt.Rows[r][i].ToString();
                                excelRange.EntireColumn.AutoFit();
                            }

                            excelRange.EntireColumn.AutoFit();

                            ascii++;

                        }

                        excelRange = xlSheet.get_Range("B4", colData);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                        ClassMsg.DialogInfomation("Export " + strName + " to excel successed.");

                        Cursor.Current = Cursors.Default;

                        xlBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);

                        xlApp.Visible = true;
                    }
                }
                return true;
            });
        }
        #endregion

        #region Export master data to excel by data datagrid view.
        public static string OnExportDataToDatagridview(DataGridView dgvData)
        {
            try
            {
                if (dgvData.DataSource != null && dgvData.Rows.Count > 0)
                {
                    // Create the CSV file to which grid data will be exported.
                    SaveFileDialog sv = new SaveFileDialog();
                    //sv.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                    sv.Filter = "Excel files (*.xls)|*.xls|Text files (*.txt)|*.txt|CSV file (*.csv)|*.csv|All files (*.*)|*.*";
                    if (sv.ShowDialog() == DialogResult.OK)
                    {
                        StreamWriter sw = new StreamWriter(sv.FileName, false, Encoding.Unicode);
                        // First we will write the headers.
                        for (int i = 0; i < dgvData.Columns.Count; i++)
                        {
                            if (dgvData.Columns[i].Visible == true)
                            {
                                sw.Write(dgvData.Columns[i].HeaderText);
                                if (i < dgvData.Columns.Count - 1)
                                {
                                    sw.Write("\t");
                                }
                            }
                        }
                        sw.Write(sw.NewLine);
                        // Now write all the rows.
                        foreach (DataGridViewRow dr in dgvData.Rows)
                        {
                            for (int i = 0; i < dgvData.Columns.Count; i++)
                            {
                                if (dgvData.Columns[i].Visible == true)
                                {
                                    if (dr.Cells[i].Value != null)
                                    {
                                        if (dr.Cells[i].GetType().ToString() == "System.DateTime")
                                        {
                                            sw.Write(Convert.ToDateTime(dr.Cells[i].Value).ToString("dd/MM/yyyy HH:mm:ss"));
                                        }
                                        else
                                        {
                                            sw.Write("=\"" + dr.Cells[i].Value.ToString().Replace("\r\n", "") + "\"");
                                        }
                                    }
                                    if (i < dgvData.Columns.Count - 1)
                                    {
                                        sw.Write("\t");
                                    }
                                }
                            }
                            sw.Write(sw.NewLine);
                        }
                        sw.Close();
                        return "Data Export completed";
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region Export master data to excel by data advan.
        public static void OnExportDataToExcel(string strName, DataGridView dgv)
        {
            if (dgv.DataSource != null && dgv.Rows.Count > 0)
            {
                Excel.Application xlApp;
                Excel._Workbook xlBook;
                Excel._Worksheet xlSheet;
                Excel.Range excelRange;
                object missing = System.Reflection.Missing.Value;

                if (ClassMsg.DialogConfirm("Do you want to export " + strName + " to excel ?") == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;

                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.FileName = strName + ".xls";
                    sfd.Filter = "Excel File | *.xls;";
                    if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {

                        xlApp = new Excel.Application();
                        xlApp.Visible = false;

                        xlApp.UserControl = false;

                        xlSheet = null;
                        excelRange = null;

                        xlBook = xlApp.Workbooks.Add(missing);
                        Excel.Sheets sheets = xlBook.Worksheets;

                        sheets.Add(System.Type.Missing, xlBook.Sheets[3], 1, Excel.XlSheetType.xlWorksheet);

                        //xlSheet.
                        xlSheet = (Excel._Worksheet)xlBook.Sheets[1];
                        xlSheet.Activate();
                        xlSheet.Name = strName;

                        // Header
                        excelRange = xlSheet.get_Range("B2", "B2");
                        excelRange.Font.Size = 14;
                        excelRange.Font.Name = "Arial";
                        excelRange.Font.Color = Color.Black.ToArgb();
                        excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                        excelRange.Value2 = strName;


                        // Column Name
                        int ascii = 66;
                        string colColumn = string.Empty;
                        string colData = string.Empty;
                        //DataTable dt = ((DataTable)dgv.DataSource).Copy();
                        for (int i = 0; i < dgv.Columns.Count; i++)
                        {

                            colColumn = char.ConvertFromUtf32(ascii) + "4";

                            excelRange = xlSheet.get_Range(colColumn, colColumn);
                            excelRange.Font.Size = 12;
                            excelRange.Font.Name = "Arial";
                            excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                            excelRange.Interior.Color = Color.FromArgb(192, 192, 192);
                            excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                            excelRange.Value2 = dgv.Columns[i].HeaderText.ToString();
                            //excelRange.Value2 = dt.Columns[i].Caption.ToString();

                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                            for (int r = 0; r < dgv.Rows.Count; r++)
                            {
                                colData = char.ConvertFromUtf32(ascii) + (r + 5);
                                excelRange = xlSheet.get_Range(colData, colData);
                                excelRange.Font.Size = 10;
                                excelRange.Font.Name = "Arial";
                                excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                excelRange.HorizontalAlignment = Excel.Constants.xlLeft;
                                if (dgv.Rows[r].Cells[i].Value == null)
                                {
                                    excelRange.Value2 = null;
                                }
                                else
                                {
                                    excelRange.Value2 = dgv.Rows[r].Cells[i].Value.ToString();
                                }
                                excelRange.EntireColumn.AutoFit();
                            }

                            excelRange.EntireColumn.AutoFit();

                            ascii++;

                        }

                        excelRange = xlSheet.get_Range("B4", colData);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                        ClassMsg.DialogInfomation("Export " + strName + " to excel successed.");

                        Cursor.Current = Cursors.Default;

                        xlBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);

                        xlApp.Visible = true;

                    }
                }
            }
        }
        public static void OnExportDataToExcel_SheetName(string strName, string strSheet, DataGridView dgv)
        {
            if (dgv.DataSource != null && dgv.Rows.Count > 0)
            {
                Excel.Application xlApp;
                Excel._Workbook xlBook;
                Excel._Worksheet xlSheet;
                Excel.Range excelRange;
                object missing = System.Reflection.Missing.Value;

                if (ClassMsg.DialogConfirm("Do you want to export " + strName + " to excel ?") == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;

                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.FileName = strName + ".xls";
                    sfd.Filter = "Excel File | *.xls;";
                    if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {

                        xlApp = new Excel.Application();
                        xlApp.Visible = false;

                        xlApp.UserControl = false;

                        xlSheet = null;
                        excelRange = null;

                        xlBook = xlApp.Workbooks.Add(missing);
                        Excel.Sheets sheets = xlBook.Worksheets;

                        sheets.Add(System.Type.Missing, xlBook.Sheets[3], 1, Excel.XlSheetType.xlWorksheet);

                        //xlSheet.
                        xlSheet = (Excel._Worksheet)xlBook.Sheets[1];
                        xlSheet.Activate();
                        xlSheet.Name = strSheet;

                        // Header
                        excelRange = xlSheet.get_Range("B2", "B2");
                        excelRange.Font.Size = 14;
                        excelRange.Font.Name = "Arial";
                        excelRange.Font.Color = Color.Black.ToArgb();
                        excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                        excelRange.Value2 = strName;


                        // Column Name
                        int ascii = 66;
                        string colColumn = string.Empty;
                        string colData = string.Empty;
                        //DataTable dt = ((DataTable)dgv.DataSource).Copy();
                        for (int i = 0; i < dgv.Columns.Count; i++)
                        {

                            colColumn = char.ConvertFromUtf32(ascii) + "4";

                            excelRange = xlSheet.get_Range(colColumn, colColumn);
                            excelRange.Font.Size = 12;
                            excelRange.Font.Name = "Arial";
                            excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                            excelRange.Interior.Color = Color.FromArgb(192, 192, 192);
                            excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                            excelRange.Value2 = dgv.Columns[i].HeaderText.ToString();
                            //excelRange.Value2 = dt.Columns[i].Caption.ToString();

                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                            for (int r = 0; r < dgv.Rows.Count; r++)
                            {
                                colData = char.ConvertFromUtf32(ascii) + (r + 5);
                                excelRange = xlSheet.get_Range(colData, colData);
                                excelRange.Font.Size = 10;
                                excelRange.Font.Name = "Arial";
                                excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                excelRange.HorizontalAlignment = Excel.Constants.xlLeft;
                                if (dgv.Rows[r].Cells[i].Value == null)
                                {
                                    excelRange.Value2 = null;
                                }
                                else
                                {
                                    excelRange.Value2 = dgv.Rows[r].Cells[i].Value.ToString();
                                }
                                excelRange.EntireColumn.AutoFit();
                            }

                            excelRange.EntireColumn.AutoFit();

                            ascii++;

                        }

                        excelRange = xlSheet.get_Range("B4", colData);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                        ClassMsg.DialogInfomation("Export " + strName + " to excel successed.");

                        Cursor.Current = Cursors.Default;

                        xlBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);

                        xlApp.Visible = true;

                    }
                }
            }
        }
        public static void OnExportDataToExcel_Extention(string strName, string strSheet, DataGridView dgv)
        {
            if (dgv.DataSource != null && dgv.Rows.Count > 0)
            {
                Excel.Application xlApp;
                Excel._Workbook xlBook;
                Excel._Worksheet xlSheet;
                Excel.Range excelRange;
                object missing = System.Reflection.Missing.Value;

                if (ClassMsg.DialogConfirm("Do you want to export " + strName + " to excel ?")
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;

                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.FileName = strName + ".xls";
                    sfd.Filter = "Excel File | *.xls;";
                    if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {

                        xlApp = new Excel.Application();
                        xlApp.Visible = false;

                        xlApp.UserControl = false;

                        xlSheet = null;
                        excelRange = null;

                        xlBook = xlApp.Workbooks.Add(missing);
                        Excel.Sheets sheets = xlBook.Worksheets;

                        sheets.Add(System.Type.Missing, xlBook.Sheets[3], 1, Excel.XlSheetType.xlWorksheet);

                        //xlSheet.
                        xlSheet = (Excel._Worksheet)xlBook.Sheets[1];
                        xlSheet.Activate();
                        xlSheet.Name = strSheet;

                        // Header
                        excelRange = xlSheet.get_Range("A2", "A2");
                        excelRange.Font.Size = 14;
                        excelRange.Font.Name = "Arial";
                        excelRange.Font.Color = Color.Black.ToArgb();
                        excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                        excelRange.Value2 = strName;


                        // Column Name
                        int ascii = 65;
                        string colColumn = string.Empty;
                        string colData = string.Empty;
                        //DataTable dt = ((DataTable)dgv.DataSource).Copy();
                        for (int i = 0; i < dgv.Columns.Count; i++)
                        {

                            colColumn = char.ConvertFromUtf32(ascii) + "4";

                            excelRange = xlSheet.get_Range(colColumn, colColumn);
                            excelRange.Font.Size = 12;
                            excelRange.Font.Name = "Arial";
                            excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                            excelRange.Interior.Color = Color.FromArgb(192, 192, 192);
                            excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                            excelRange.Value2 = dgv.Columns[i].HeaderText.ToString();
                            //excelRange.Value2 = dt.Columns[i].Caption.ToString();

                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                            for (int r = 0; r < dgv.Rows.Count; r++)
                            {
                                colData = char.ConvertFromUtf32(ascii) + (r + 5);
                                excelRange = xlSheet.get_Range(colData, colData);
                                excelRange.Font.Size = 10;
                                excelRange.Font.Name = "Arial";
                                excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                excelRange.HorizontalAlignment = Excel.Constants.xlLeft;
                                if (dgv.Rows[r].Cells[i].Value == null)
                                {
                                    excelRange.Value2 = null;
                                }
                                else
                                {
                                    excelRange.Value2 = dgv.Rows[r].Cells[i].Value.ToString();
                                }
                                excelRange.EntireColumn.AutoFit();
                            }

                            excelRange.EntireColumn.AutoFit();

                            ascii++;

                        }

                        excelRange = xlSheet.get_Range("A4", colData);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                        excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                        ClassMsg.DialogInfomation("Export " + strName + " to excel successed.");

                        Cursor.Current = Cursors.Default;

                        xlBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);

                        xlApp.Visible = true;

                    }
                }
            }
        }
        #endregion

        #region Export master data to excel by data advan for transaction.
        //Start record at B and not parameter Sheet name.
        public static bool OnExportDataToExcelFiles(string strName, DataGridView dgv)
        {
            if (dgv.DataSource != null && dgv.Rows.Count > 0)
            {
                return WorkData.Work.Working(delegate()
                {
                    Excel.Application xlApp;
                    Excel._Workbook xlBook;
                    Excel._Worksheet xlSheet;
                    Excel.Range excelRange;
                    object missing = System.Reflection.Missing.Value;

                    if (ClassMsg.DialogConfirm("Do you want to export " + strName + " to excel ?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        Cursor.Current = Cursors.WaitCursor;

                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.FileName = strName + ".xls";
                        sfd.Filter = "Excel File | *.xls;";
                        if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            xlApp = new Excel.Application();
                            xlApp.Visible = false;

                            xlApp.UserControl = false;

                            xlSheet = null;
                            excelRange = null;

                            xlBook = xlApp.Workbooks.Add(missing);
                            Excel.Sheets sheets = xlBook.Worksheets;

                            sheets.Add(System.Type.Missing, xlBook.Sheets[3], 1, Excel.XlSheetType.xlWorksheet);

                            //xlSheet.
                            xlSheet = (Excel._Worksheet)xlBook.Sheets[1];
                            xlSheet.Activate();
                            xlSheet.Name = strName;

                            // Header
                            excelRange = xlSheet.get_Range("B2", "B2");
                            excelRange.Font.Size = 14;
                            excelRange.Font.Name = "Arial";
                            excelRange.Font.Color = Color.Black.ToArgb();
                            excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                            excelRange.Value2 = strName;


                            // Column Name
                            int ascii = 66;
                            string colColumn = string.Empty;
                            string colData = string.Empty;
                            //DataTable dt = ((DataTable)dgv.DataSource).Copy();
                            for (int i = 0; i < dgv.Columns.Count; i++)
                            {

                                colColumn = char.ConvertFromUtf32(ascii) + "4";

                                excelRange = xlSheet.get_Range(colColumn, colColumn);
                                excelRange.Font.Size = 12;
                                excelRange.Font.Name = "Arial";
                                excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                excelRange.Interior.Color = Color.FromArgb(192, 192, 192);
                                excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                                excelRange.Value2 = dgv.Columns[i].HeaderText.ToString();
                                //excelRange.Value2 = dt.Columns[i].Caption.ToString();

                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                                for (int r = 0; r < dgv.Rows.Count; r++)
                                {
                                    colData = char.ConvertFromUtf32(ascii) + (r + 5);
                                    excelRange = xlSheet.get_Range(colData, colData);
                                    excelRange.Font.Size = 10;
                                    excelRange.Font.Name = "Arial";
                                    excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                    excelRange.HorizontalAlignment = Excel.Constants.xlLeft;
                                    if (dgv.Rows[r].Cells[i].Value == null)
                                    {
                                        excelRange.Value2 = null;
                                    }
                                    else
                                    {
                                        excelRange.Value2 = dgv.Rows[r].Cells[i].Value.ToString();
                                    }
                                    excelRange.EntireColumn.AutoFit();
                                }

                                excelRange.EntireColumn.AutoFit();

                                ascii++;

                            }

                            excelRange = xlSheet.get_Range("B4", colData);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                            ClassMsg.DialogInfomation("Export " + strName + " to excel successed.");

                            Cursor.Current = Cursors.Default;

                            xlBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);

                            xlApp.Visible = true;

                        }
                    }
                    return true;
                });
            }
            return true;
        }
        //Start record at B and parameter Sheet name.
        public static bool OnExportDataToExcelFiles_SheetName(string strName, string strSheet, DataGridView dgv)
        {
            if (dgv.DataSource != null && dgv.Rows.Count > 0)
            {
                return WorkData.Work.Working(delegate()
                {
                    Excel.Application xlApp;
                    Excel._Workbook xlBook;
                    Excel._Worksheet xlSheet;
                    Excel.Range excelRange;
                    object missing = System.Reflection.Missing.Value;

                    if (ClassMsg.DialogConfirm("Do you want to export " + strName + " to excel ?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        Cursor.Current = Cursors.WaitCursor;

                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.FileName = strName + ".xls";
                        sfd.Filter = "Excel File | *.xls;";
                        if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            xlApp = new Excel.Application();
                            xlApp.Visible = false;

                            xlApp.UserControl = false;

                            xlSheet = null;
                            excelRange = null;

                            xlBook = xlApp.Workbooks.Add(missing);
                            Excel.Sheets sheets = xlBook.Worksheets;

                            sheets.Add(System.Type.Missing, xlBook.Sheets[3], 1, Excel.XlSheetType.xlWorksheet);

                            //xlSheet.
                            xlSheet = (Excel._Worksheet)xlBook.Sheets[1];
                            xlSheet.Activate();
                            xlSheet.Name = strSheet;

                            // Header
                            excelRange = xlSheet.get_Range("B2", "B2");
                            excelRange.Font.Size = 14;
                            excelRange.Font.Name = "Arial";
                            excelRange.Font.Color = Color.Black.ToArgb();
                            excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                            excelRange.Value2 = strName;


                            // Column Name
                            int ascii = 66;
                            string colColumn = string.Empty;
                            string colData = string.Empty;
                            //DataTable dt = ((DataTable)dgv.DataSource).Copy();
                            for (int i = 0; i < dgv.Columns.Count; i++)
                            {

                                colColumn = char.ConvertFromUtf32(ascii) + "4";

                                excelRange = xlSheet.get_Range(colColumn, colColumn);
                                excelRange.Font.Size = 12;
                                excelRange.Font.Name = "Arial";
                                excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                excelRange.Interior.Color = Color.FromArgb(192, 192, 192);
                                excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                                excelRange.Value2 = dgv.Columns[i].HeaderText.ToString();
                                //excelRange.Value2 = dt.Columns[i].Caption.ToString();

                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                                for (int r = 0; r < dgv.Rows.Count; r++)
                                {
                                    colData = char.ConvertFromUtf32(ascii) + (r + 5);
                                    excelRange = xlSheet.get_Range(colData, colData);
                                    excelRange.Font.Size = 10;
                                    excelRange.Font.Name = "Arial";
                                    excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                    excelRange.HorizontalAlignment = Excel.Constants.xlLeft;
                                    if (dgv.Rows[r].Cells[i].Value == null)
                                    {
                                        excelRange.Value2 = null;
                                    }
                                    else
                                    {
                                        excelRange.Value2 = dgv.Rows[r].Cells[i].Value.ToString();
                                    }
                                    excelRange.EntireColumn.AutoFit();
                                }

                                excelRange.EntireColumn.AutoFit();

                                ascii++;

                            }

                            excelRange = xlSheet.get_Range("B4", colData);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                            ClassMsg.DialogInfomation("Export " + strName + " to excel successed.");

                            Cursor.Current = Cursors.Default;

                            xlBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);

                            xlApp.Visible = true;

                        }
                    }
                    return true;
                });
            }
            return true;
        }
        //Start record at A and parameter Sheet name.
        public static bool OnExportDataToExcelFiles_Extention(string strName, string strSheet, DataGridView dgv)
        {
            if (dgv.DataSource != null && dgv.Rows.Count > 0)
            {
                return WorkData.Work.Working(delegate()
                {
                    Excel.Application xlApp;
                    Excel._Workbook xlBook;
                    Excel._Worksheet xlSheet;
                    Excel.Range excelRange;
                    object missing = System.Reflection.Missing.Value;

                    if (ClassMsg.DialogConfirm("Do you want to export " + strName + " to excel ?")
                        == System.Windows.Forms.DialogResult.Yes)
                    {
                        Cursor.Current = Cursors.WaitCursor;

                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.FileName = strName + ".xls";
                        sfd.Filter = "Excel File | *.xls;";
                        if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            xlApp = new Excel.Application();
                            xlApp.Visible = false;

                            xlApp.UserControl = false;

                            xlSheet = null;
                            excelRange = null;

                            xlBook = xlApp.Workbooks.Add(missing);
                            Excel.Sheets sheets = xlBook.Worksheets;

                            sheets.Add(System.Type.Missing, xlBook.Sheets[3], 1, Excel.XlSheetType.xlWorksheet);

                            //xlSheet.
                            xlSheet = (Excel._Worksheet)xlBook.Sheets[1];
                            xlSheet.Activate();
                            xlSheet.Name = strSheet;

                            // Header
                            excelRange = xlSheet.get_Range("A2", "A2");
                            excelRange.Font.Size = 14;
                            excelRange.Font.Name = "Arial";
                            excelRange.Font.Color = Color.Black.ToArgb();
                            excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                            excelRange.Value2 = strName;


                            // Column Name
                            int ascii = 65;
                            string colColumn = string.Empty;
                            string colData = string.Empty;
                            //DataTable dt = ((DataTable)dgv.DataSource).Copy();
                            for (int i = 0; i < dgv.Columns.Count; i++)
                            {

                                colColumn = char.ConvertFromUtf32(ascii) + "4";

                                excelRange = xlSheet.get_Range(colColumn, colColumn);
                                excelRange.Font.Size = 12;
                                excelRange.Font.Name = "Arial";
                                excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                excelRange.Interior.Color = Color.FromArgb(192, 192, 192);
                                excelRange.HorizontalAlignment = Excel.Constants.xlCenter;
                                excelRange.Value2 = dgv.Columns[i].HeaderText.ToString();
                                //excelRange.Value2 = dt.Columns[i].Caption.ToString();

                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                                excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                                for (int r = 0; r < dgv.Rows.Count; r++)
                                {
                                    colData = char.ConvertFromUtf32(ascii) + (r + 5);
                                    excelRange = xlSheet.get_Range(colData, colData);
                                    excelRange.Font.Size = 10;
                                    excelRange.Font.Name = "Arial";
                                    excelRange.Font.Color = Color.FromArgb(0, 0, 0);
                                    excelRange.HorizontalAlignment = Excel.Constants.xlLeft;
                                    if (dgv.Rows[r].Cells[i].Value == null)
                                    {
                                        excelRange.Value2 = null;
                                    }
                                    else
                                    {
                                        excelRange.Value2 = dgv.Rows[r].Cells[i].Value.ToString();
                                    }
                                    excelRange.EntireColumn.AutoFit();
                                }

                                excelRange.EntireColumn.AutoFit();

                                ascii++;

                            }

                            excelRange = xlSheet.get_Range("A4", colData);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Color = Color.FromArgb(0, 0, 0);
                            excelRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Color = Color.FromArgb(0, 0, 0);

                            ClassMsg.DialogInfomation("Export " + strName + " to excel successed.");

                            Cursor.Current = Cursors.Default;

                            xlBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);

                            xlApp.Visible = true;

                        }
                    }
                    return true;
                });
            }
            return true;
        }
        #endregion
    }
}